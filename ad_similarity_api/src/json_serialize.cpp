#include "json_serialize.h"
#include <sstream>
#include <iomanip>
#include <iostream>

static void BaseResult2Json(const BaseResult &result, Json::Value *json_item) {
    Json::Value &data_item = *json_item;
    data_item["image_width"] = result.image_width;
    data_item["image_height"] = result.image_height;
    data_item["errorcode"] = result.errorcode;
    data_item["errormsg"] = result.errormsg;
    data_item["latency"] = result.latency;
}

std::string DetectorResult2Json(const DetectorResult &result) {
    Json::Value root;
    Json::Value data_item;
    BaseResult2Json(result.base, &data_item);

    Json::Value json_regions;
    for (const BoxItem &box_item : result.Items) {
        Json::Value item;
        Json::Value json_points;
        const DetBox &det_box = box_item.box;
        for (const TPoint2f &pt : det_box.rect.Points()) {
            json_points.append((int)pt.x);
            json_points.append((int)pt.y);
        }
        item["name"] = box_item.text;
        item["type"] = det_box.type;
        item["confidence"] = det_box.conf;
        item["bbox"] = json_points;
        json_regions.append(item);
    }
    data_item["regions"] = json_regions;

    root["data"] = data_item;
    // std::string out = root.toStyledString();
    Json::StreamWriterBuilder wbuilder;
    wbuilder["commentStyle"] = "None";
    wbuilder["indentation"] = ""; //The JSON document is written in a single line

    std::string out = Json::writeString(wbuilder, root);
    return out;
}

std::string CNNClassifyResult2Json(const CNNClassifyResult &result) {
    Json::Value root;
    Json::Value data_item;
    BaseResult2Json(result.base, &data_item);

    data_item["type"] = result.predict.first;
    data_item["confidence"] = result.predict.second;

    root["data"] = data_item;
    // std::string out = root.toStyledString();
    Json::StreamWriterBuilder wbuilder;
    wbuilder["commentStyle"] = "None";
    wbuilder["indentation"] = ""; //The JSON document is written in a single line

    std::string out = Json::writeString(wbuilder, root);
    return out;
}

float round(float number, unsigned int bits) {
    std::stringstream ss;
    ss << std::fixed << std::setprecision(bits) << number;
    ss >> number;
    return number;
}

std::string AdSimilarityResult2Json(const AdSimilarityResult &result) {
    Json::Value data_item;
    BaseResult2Json(result.base, &data_item);

    Json::Value hash_vecs;
    for(int i = 0; i < result.hash_predicts.size(); i++){
        Json::Value hash_vec;
        for(int j = 0; j < result.hash_predicts[i].size(); j++)
            hash_vec.append(result.hash_predicts[i][j]);
        hash_vecs.append(hash_vec);
    }
    data_item["hash_result"] = hash_vecs;
    Json::Value float_vecs;
    for(int i = 0; i < result.float_predicts.size(); i++){
        Json::Value float_vec;
        for(int j = 0; j < result.float_predicts[i].size(); j++){
            float_vec.append(round(result.float_predicts[i][j],4));
            //std::cout << round(result.float_predicts[i][j],4) << std::endl;
        }
        float_vecs.append(float_vec);
    }
    data_item["float_result"] = float_vecs;
    Json::StreamWriterBuilder wbuilder;
    wbuilder["commentStyle"] = "None";
    wbuilder["indentation"] = ""; //The JSON document is written in a single line

    std::string out = Json::writeString(wbuilder, data_item);
    return out;
}

