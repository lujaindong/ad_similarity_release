#include "caffe_model_config.h"
#include <boost/filesystem.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
namespace OM {
CaffeModelConfig::CaffeModelConfig(std::string config_file) {
    // if (boost::filesystem::exists(config_file))
    {
        boost::filesystem::path config_path(config_file);
        std::string root = config_path.parent_path().string();
        cv::FileStorage config(config_file, cv::FileStorage::READ);
        this->model_file   = root + "/" + (std::string) config[ "PROTOTXT" ];
        this->weights_file = root + "/" + (std::string) config[ "MODEL" ];
        this->mean_value   = (std::string) config[ "MEAN_VALUE" ];
        this->var_value   = (std::string) config[ "VAR_VALUE" ];
    }
}
}
