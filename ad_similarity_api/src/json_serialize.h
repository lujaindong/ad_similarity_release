#pragma once
#include "types.h"
#include <json/json.h>
std::string DetectorResult2Json(const DetectorResult &result);
std::string CNNClassifyResult2Json(const CNNClassifyResult &result);
std::string AdSimilarityResult2Json(const AdSimilarityResult &result);
