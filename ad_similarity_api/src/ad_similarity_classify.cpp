#include "ad_similarity_classify.h"
#include "api/caffe_forward.h"
#include "api/blob_data.h"
#include "caffe_model_config.h"
#include <opencv2/opencv.hpp>
#include <memory>
#include <json/json.h>
#include <boost/date_time/posix_time/posix_time.hpp>
#include "json_serialize.h"
#include <boost/filesystem.hpp>
#include <sstream>
using caffe::CaffeForward;
using caffe::BlobData;

namespace OM {
typedef struct AdSimilarityPrivate {
    std::unique_ptr<CaffeForward> classify_net;
    std::unique_ptr<CaffeModelConfig> config;
} AdSimilarityPrivate;

AdSimilarityClassify::AdSimilarityClassify() {
    priv = new AdSimilarityPrivate();
}

AdSimilarityClassify::~AdSimilarityClassify() {
    delete priv;
}

int AdSimilarityClassify::Init(std::string model_config, int gpu_id) {
    priv->config.reset(new CaffeModelConfig(model_config));
    priv->classify_net.reset(new CaffeForward(priv->config->model_file, priv->config->weights_file, priv->config->mean_value, priv->config->var_value, gpu_id));
    return 0;
}

std::vector<std::vector<float>> AdSimilarityClassify::Predict(const std::vector<cv::Mat> &images, int type) {
    std::map<string, std::shared_ptr<BlobData>> predicts;
    priv->classify_net->Forward(images);
    //std::cout << "success"<< std::endl; 
    std::string blob_name = "";
    if (type == 0){
        blob_name = "pca_itq_output";
    }
    else if (type == 1){
        blob_name = "output_l2_norm";
    }
    std::vector<std::string> names;
    names.push_back(blob_name);
    predicts = priv->classify_net->GetBlobs(names);
    std::shared_ptr<BlobData> blob_ptr = predicts[blob_name];
    //shape
    //std::vector<int> shape = blob_ptr->shape();
    //std::ostringstream stream;
    //for (int i = 0; i < shape.size(); ++i) {
    //      stream << shape[i] << " ";
    //}
    //std::cout << stream.str() << std::endl;  
    //shape
    
    //get result
    std::vector<std::vector<float>> result;
    int num = blob_ptr->num();
    for(int i=0; i<num; i++){
        const float* begin = blob_ptr->data_pointer() + blob_ptr->offset(i);
        const float* end = blob_ptr->data_pointer() + blob_ptr->offset(i+1);
        result.push_back(std::vector<float>(begin, end));
    }
    if (type == 0){
        for(int i = 0; i < result.size(); i ++ ){
            for(int j = 0; j < result[i].size(); j ++){
                if (result[i][j] < 0) {
                    result[i][j] = 0;
                }else{
                    result[i][j] = 1;
                }
            }
        }
    }   
    return result;
}

 
AdSimilarityResult AdSimilarityClassify::PredictFromImageData(const std::vector<std::string> &data_buffer, int type) {
    AdSimilarityResult result;
    try {
        boost::posix_time::ptime start_cpu = boost::posix_time::microsec_clock::local_time();
        std::vector<cv::Mat> images;
        for(int i = 0; i < data_buffer.size(); i++){ 
            std::vector<uchar> image_data(data_buffer[i].data(),
                                      data_buffer[i].data() + data_buffer[i].size());
            cv::Mat image = cv::imdecode(image_data, CV_LOAD_IMAGE_ANYCOLOR);
            images.push_back(image);
        }
        //std::cout << images.size() << std::endl;
        if (type == 0){
            std::vector<std::vector<float>> hash_predict = Predict(images, type);
            std::vector<std::vector<int>> hash_predict_int;
            hash_predict_int.resize(hash_predict.size());
            for(int i = 0; i < hash_predict.size(); i++)
                hash_predict_int[i].resize(hash_predict[i].size());
            for(int i = 0; i < hash_predict.size(); i ++ ){
                for(int j = 0; j <hash_predict[i].size(); j ++){
                    hash_predict_int[i][j] = int(hash_predict[i][j]);
                }
            }
            result.hash_predicts = hash_predict_int;
        }else if(type == 1){
            std::vector<std::vector<float>> float_predict = Predict(images, type);
            result.float_predicts = float_predict;
        }
        result.base.image_width = -1;
        result.base.image_height = -1;
        boost::posix_time::ptime stop_cpu = boost::posix_time::microsec_clock::local_time();
        float time_span = (stop_cpu - start_cpu).total_microseconds() / 1000;
        result.base.latency = time_span;
    } catch (cv::Exception &e) {
        result.base.errormsg = e.err + " in " + e.func;
    }

    return result;
}

std::string AdSimilarityClassify::PredictFromImageDataJson(const std::vector<std::string> &data_buffer, int type) {
    AdSimilarityResult result;
    result = PredictFromImageData(data_buffer, type);
    return AdSimilarityResult2Json(result);
}
} // namespace OM

AdSimilarityEngine *CreateAdSimilarityEngine(std::string model_config, int  gpu_id) {
    AdSimilarityEngine *engine = new AdSimilarityEngine();
    engine->p_handle = 0;
    std::unique_ptr<OM::AdSimilarityClassify> ad_similarity_engine(new OM::AdSimilarityClassify());
    try {
        ad_similarity_engine->Init(model_config, gpu_id);
        engine->p_handle = (intptr_t)ad_similarity_engine.release();
    } catch (...) {
        std::cout << "ad similarity model init error" << std::endl;
        engine->p_handle = 0;
    }
    return engine;
}

void ReleaseAdSimilarityEngine(AdSimilarityEngine *p_engine) {
    if (p_engine != nullptr) {
        std::unique_ptr<OM::AdSimilarityClassify> logo_engine((OM::AdSimilarityClassify *)(p_engine->p_handle));
        p_engine->p_handle = 0;
        delete p_engine;
    }
}

std::string AdSimilarityEnginePredict(AdSimilarityEngine *p_engine, const std::vector<std::string> &images_buffer, int type) {
    AdSimilarityResult result;
    OM::AdSimilarityClassify *ad_similarity_engine = (OM::AdSimilarityClassify *)(p_engine->p_handle);
    if (ad_similarity_engine == nullptr) {
        result.base.errormsg = "engine not initialized";
        result.base.errorcode = -1;
    } else {
        result = ad_similarity_engine->PredictFromImageData(images_buffer, type);
    }
    return AdSimilarityResult2Json(result);
}
