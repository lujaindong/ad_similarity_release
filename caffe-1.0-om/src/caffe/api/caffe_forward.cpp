#include "caffe_forward.h"
#include <caffe/caffe.hpp>

#ifdef USE_OPENCV
#include <opencv2/opencv.hpp>
#endif // USE_OPENCV

#include <algorithm>
#include <iosfwd>
#include <memory>
#include <string>
#include <utility>
#include <vector>
//#include <boost/date_time/posix_time/posix_time.hpp>

using namespace caffe; // NOLINT(build/namespaces)
using std::string;
using namespace std;

typedef struct CaffeForwardPrivate {
    std::shared_ptr<Net<float>> net_;
    std::string init_model_file;
    cv::Size input_geometry_;
    int num_channels_;
    int num_;
    cv::Mat mean_;
    cv::Mat var_;
    int gpu_id;
    vector<float> mean_values;
    vector<float> var_values;
} CaffeForwardPrivate;

/* Pair (label, confidence) representing a prediction. */
void init_caffe_global(int gpu) {
#ifdef CPU_ONLY
    gpu = -1;
#endif

    if (gpu < 0) {
        //LOG(INFO) << "using CPU";
        Caffe::set_mode(Caffe::CPU);
    } else {
        //LOG(INFO) << "using GPU " << gpu;
        Caffe::SetDevice(gpu);
        Caffe::set_mode(Caffe::GPU);
    }
}
namespace caffe {

CaffeForward::CaffeForward() {
    priv = new CaffeForwardPrivate();
}

CaffeForward::CaffeForward(const string &model_file,
                           const string &trained_file,
                           const std::string &mean_value, 
                           const std::string &var_value, int gpu /*= -1*/) {

    priv = new CaffeForwardPrivate();

    priv->gpu_id = gpu;
    init_caffe_global(priv->gpu_id);

    priv->init_model_file = model_file;

    /* Load the network. */
    priv->net_.reset(new Net<float>(model_file, TEST));
    priv->net_->CopyTrainedLayersFrom(trained_file);

    CHECK_EQ(priv->net_->num_inputs(), 1) << "Network should have exactly one input.";

    Blob<float> *input_layer = priv->net_->input_blobs()[0];

    priv->num_channels_ = input_layer->channels();
    priv->num_ = input_layer->num();
    CHECK(priv->num_channels_ == 3 || priv->num_channels_ == 1) << "Input layer should have 1 or 3 channels.";
    priv->input_geometry_ = cv::Size(input_layer->width(), input_layer->height());

    /* Load the binaryproto mean file. */
    SetMean(mean_value);
    /* Load the binaryproto var file. */
    SetVar(var_value);

}

CaffeForward::~CaffeForward() {
    delete priv;
}

cv::Size CaffeForward::GetInputSize() {
    return priv->input_geometry_;
}

void CaffeForward::SetInputSize(cv::Size size) {
    priv->input_geometry_ = size;
    if (priv->input_geometry_ != priv->mean_.size()) {
        std::vector<cv::Mat> channels_mean;
        std::vector<cv::Mat> channels_var;
        for (int i = 0; i < priv->num_channels_; ++i) {
            /* Extract an individual channel. */
            cv::Mat channel_mean(priv->input_geometry_.height, priv->input_geometry_.width, CV_32FC1,
                            cv::Scalar(priv->mean_values[i]));
            channels_mean.push_back(channel_mean);
            cv::Mat channel_var(priv->input_geometry_.height, priv->input_geometry_.width, CV_32FC1,
                            cv::Scalar(priv->var_values[i]));
            channels_var.push_back(channel_var);            
        }
        cv::merge(channels_mean, priv->mean_);
        cv::merge(channels_var, priv->var_);

    }
}

int CaffeForward::Channels() {
    return priv->num_channels_;
}

std::shared_ptr<CaffeForward> CaffeForward::cloneShared() {
    std::shared_ptr<CaffeForward> net = std::shared_ptr<CaffeForward>(new CaffeForward());

    net->priv->net_.reset(new Net<float>(priv->init_model_file, TEST));
    net->priv->net_->ShareTrainedLayersWith(priv->net_.get());

    net->priv->init_model_file = priv->init_model_file;

    net->priv->input_geometry_ = priv->input_geometry_;
    net->priv->num_channels_ = priv->num_channels_;
    net->priv->num_ = priv->num_;

    net->priv->mean_ = priv->mean_.clone();
    net->priv->gpu_id = priv->gpu_id;
    net->priv->mean_values = priv->mean_values;
    net->priv->var_values = priv->var_values;
    return net;
}

/* Load the mean file in binaryproto format. */
void CaffeForward::SetMean(const string &mean_value) {
    if (!mean_value.empty()) {
        stringstream ss(mean_value);
        string item;
        while (getline(ss, item, ',')) {
            float value = std::atof(item.c_str());
            priv->mean_values.push_back(value);
        }
        CHECK(priv->mean_values.size() == 1 || priv->mean_values.size() == priv->num_channels_) << "Specify either 1 mean_value or as many as channels: " << priv->num_channels_;

        std::vector<cv::Mat> channels;
        for (int i = 0; i < priv->num_channels_; ++i) {
            /* Extract an individual channel. */
            cv::Mat channel(priv->input_geometry_.height, priv->input_geometry_.width, CV_32FC1,
                            cv::Scalar(priv->mean_values[i]));
            channels.push_back(channel);
        }
        cv::merge(channels, priv->mean_);
    }
}

/* Load the var file in binaryproto format. */
void CaffeForward::SetVar(const string &var_value) {
    if (!var_value.empty()) {
        stringstream ss(var_value);
        string item;
        while (getline(ss, item, ',')) {
            float value = std::atof(item.c_str());
            priv->var_values.push_back(value);
        }
        CHECK(priv->var_values.size() == 1 || priv->var_values.size() == priv->num_channels_) << "Specify either 1 mean_value or as many as channels: " << priv->num_channels_;

        std::vector<cv::Mat> channels;
        for (int i = 0; i < priv->num_channels_; ++i) {
            /* Extract an individual channel. */
            cv::Mat channel(priv->input_geometry_.height, priv->input_geometry_.width, CV_32FC1,
                            cv::Scalar(priv->var_values[i]));
            channels.push_back(channel);
        }
        cv::merge(channels, priv->var_);
    }
}

std::map<string, std::shared_ptr<BlobData>> CaffeForward::Forward(std::vector<cv::Mat> images) {
    assert(images.size() > 0);
    assert(priv->input_geometry_.height > 0 && priv->input_geometry_.width > 0);

    init_caffe_global(priv->gpu_id);

    //boost::posix_time::ptime reshape_start_cpu = boost::posix_time::microsec_clock::local_time();
    priv->num_ = images.size();

    Blob<float> *input_layer = priv->net_->input_blobs()[0];
    input_layer->Reshape(priv->num_, priv->num_channels_,
                         priv->input_geometry_.height, priv->input_geometry_.width);

    /* Forward dimension change to all layers. */
    priv->net_->Reshape();
    //boost::posix_time::ptime reshape_stop_cpu = boost::posix_time::microsec_clock::local_time();
    //float reshape_time_span = (reshape_stop_cpu - reshape_start_cpu).total_microseconds() / 1000;
    //std::cout << "reshape time= " << reshape_time_span << std::endl;

    //boost::posix_time::ptime wrap_start_cpu = boost::posix_time::microsec_clock::local_time();
    std::vector<cv::Mat> input_channels;
    WrapInputLayer(&input_channels);
    Preprocess(images, &input_channels);
    //boost::posix_time::ptime wrap_stop_cpu = boost::posix_time::microsec_clock::local_time();
    //float wrap_time_span = (wrap_stop_cpu - wrap_start_cpu).total_microseconds() / 1000;
    //std::cout << "wrap time= " << wrap_time_span << std::endl;
    
    //boost::posix_time::ptime forward_start_cpu = boost::posix_time::microsec_clock::local_time();
    priv->net_->Forward();
    //boost::posix_time::ptime forward_stop_cpu = boost::posix_time::microsec_clock::local_time();
    //float forward_time_span = (forward_stop_cpu - forward_start_cpu).total_microseconds() / 1000;
    //std::cout << "forward time= " << forward_time_span << std::endl;
    std::map<string, std::shared_ptr<BlobData>> outputs;

    const vector<int> &output_indices = priv->net_->output_blob_indices();
    for (int i = 0; i < output_indices.size(); i++) {
		int idx = output_indices[i];
        const string blob_name = priv->net_->blob_names()[idx];
        shared_ptr<Blob<float>> blob = priv->net_->blobs()[idx];

        std::shared_ptr<BlobData> block(new BlobData((blob->shape())));
        memcpy(block->data_pointer(), blob->cpu_data(), blob->count() * sizeof(float));
        outputs[blob_name] = block;
    }

    return outputs;
}

//names:代表blob的name list
//GetBlobs:支持根据blob name去提取中间的输出结果
//调用时可以先使用Forward跑一次网络
//然后使用GetBlobs提取需要的中间结果
std::map<string, std::shared_ptr<BlobData>> CaffeForward::GetBlobs(std::vector<std::string> names) {
    std::map<string, std::shared_ptr<BlobData>> outputs;
    for (int i = 0; i < names.size(); i++) {
        shared_ptr<Blob<float>> blob = priv->net_->blob_by_name(names[i]);
        std::shared_ptr<BlobData> block(new BlobData((blob->shape())));
        memcpy(block->data_pointer(), blob->cpu_data(), blob->count() * sizeof(float));
        outputs[names[i]] = block;
    }
    return outputs;
}

/* Wrap the input layer of the network in separate cv::Mat objects
 * (one per channel). This way we save one memcpy operation and we
 * don't need to rely on cudaMemcpy2D. The last preprocessing
 * operation will write the separate channels directly to the input
 * layer. */
void CaffeForward::WrapInputLayer(std::vector<cv::Mat> *input_channels) {
    Blob<float> *input_layer = priv->net_->input_blobs()[0];

    int width = input_layer->width();
    int height = input_layer->height();
    float *input_data = input_layer->mutable_cpu_data();
    for (int j = 0; j < priv->num_; ++j) {
        for (int i = 0; i < input_layer->channels(); ++i) {
            cv::Mat channel(height, width, CV_32FC1, input_data);
            input_channels->push_back(channel);
            input_data += width * height;
        }
    }
}
//素材指纹模型对图片的预处理
//BGR->RGB
//resize(224,224)
//0-255 scale to [0,1]
//减均值[0.5, 0.5, 0.5],除方差[0.5, 0.5, 0.5],等价于缩放到[-1,1]
void CaffeForward::Preprocess_step(cv::Mat &img, std::vector<cv::Mat> *input_channels, int step) {
    /* Convert the input image to the input image format of the network. */

    cv::Mat sample;
    if (img.channels() == 3 && priv->num_channels_ == 1) {
        cv::cvtColor(img, sample, cv::COLOR_BGR2GRAY);
    } else if (img.channels() == 4 && priv->num_channels_ == 1) {
        cv::cvtColor(img, sample, cv::COLOR_BGRA2GRAY);
    } else if (img.channels() == 4 && priv->num_channels_ == 3) {
        cv::cvtColor(img, sample, cv::COLOR_BGRA2BGR);
    } else if (img.channels() == 1 && priv->num_channels_ == 3) {
        cv::cvtColor(img, sample, cv::COLOR_GRAY2BGR);
    } else {
        sample = img;
        //std::cout << "sample=img" << cv::sum(sample)[0] <<" " << cv::sum(sample)[1] <<" " << cv::sum(sample)[2] << std::endl;
    }
    //BGR TO RGB
    cv::Mat rgb_sample;
    cv::cvtColor(sample, rgb_sample, CV_BGR2RGB);
    //std::cout << "cvtColor" << cv::sum(rgb_sample)[0] << std::endl;
    //resize
    cv::Mat sample_resized;
    if (sample.size() != priv->input_geometry_) {
        cv::resize(rgb_sample, sample_resized, priv->input_geometry_);
        //std::cout << "cv_resize" << cv::sum(sample_resized)[0] << std::endl;
    }
    else{
        sample_resized = rgb_sample;
    }

    cv::Mat sample_float;
    if (priv->num_channels_ == 3) {
        sample_resized.convertTo(sample_float, CV_32FC3);
        //std::cout << "cv_convert to float" << cv::sum(sample_float)[0] << std::endl;
    } else {
        sample_resized.convertTo(sample_float, CV_32FC1);
    }
    
    //scale to [0,1], divide by 255
    sample_float = sample_float/255.0;
    //sub_mean [0.5, 0.5, 0.5]
    cv::Mat sample_normalized_mean;
    if (priv->mean_.data == NULL) {
        sample_normalized_mean = sample_float;
    } else {
        cv::subtract(sample_float, priv->mean_, sample_normalized_mean);
    }
    //divide_var [0.5, 0.5, 0.5]
    cv::Mat sample_normalized;
    if (priv->var_.data == NULL) {
        sample_normalized = sample_normalized_mean;
    } else {
        cv::divide(sample_normalized_mean, priv->var_, sample_normalized);
    }
    /* This operation will write the separate BGR planes directly to the
     * input layer of the network because it is wrapped by the cv::Mat
     * objects in input_channels. */
    std::vector<cv::Mat> input_blobs;
    for (int k = 0; k < priv->num_channels_; ++k) {
        input_blobs.push_back(input_channels->at(step * priv->num_channels_ + k));
    }
    cv::split(sample_normalized, input_blobs);
}

void CaffeForward::Preprocess(std::vector<cv::Mat> images, std::vector<cv::Mat> *input_channels) {
    /* Convert the input image to the input image format of the network. */
    /* Convert the input image to the input image format of the network. */
    for (int i = 0; i < images.size(); ++i) {
        Preprocess_step(images[i], input_channels, i);
    }
}


} //namespace caffe
