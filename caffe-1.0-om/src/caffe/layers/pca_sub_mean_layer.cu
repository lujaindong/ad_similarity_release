#include <algorithm>
#include <cfloat>
#include <vector>
#include <iostream>

#include "thrust/device_vector.h"

#include "caffe/layers/pca_sub_mean_layer.hpp"
#include "caffe/util/math_functions.hpp"

namespace caffe {

template <typename Dtype>
void PcaSubMeanLayer<Dtype>::Forward_gpu(const vector<Blob<Dtype>*>& bottom,
    const vector<Blob<Dtype>*>& top) {
  const Dtype* bottom_data = bottom[0]->gpu_data();
  Dtype* top_data = top[0]->mutable_gpu_data();
  Dtype* data_mean = data_mean_.mutable_gpu_data();
  int n = bottom[0]->num();
  int d = bottom[0]->count() / n;
  //std::cout << "n=" << n << std::endl;
  //std::cout << "d=" << d << std::endl;
  //std::cout << "bottom shape = " << bottom[0]->shape_string() << std::endl;
  //std::cout << "top shape = " << top[0]->shape_string() << std::endl;
  //const Dtype* cpu_data_mean = data_mean_.cpu_data();
  //std::cout << "bottom_data" << *(cpu_data_mean+d-1) << std::endl;
  for (int i=0; i<n; ++i) {
    caffe_gpu_sub<Dtype>(d, bottom_data+i*d, data_mean, top_data+i*d);
    //std::cout << "success" << std::endl;
  }
}

//PCA sub mean Just for deploy, not BP
template <typename Dtype>
void PcaSubMeanLayer<Dtype>::Backward_gpu(const vector<Blob<Dtype>*>& top,
    const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom) {
	;
}

INSTANTIATE_LAYER_GPU_FUNCS(PcaSubMeanLayer);


}  // namespace caffe
