#include <algorithm>
#include <vector>
#include <string>
#include <iostream>
#include "caffe/layers/pca_div_var_layer.hpp"
#include "caffe/util/math_functions.hpp"
#include "caffe/util/io.hpp"

namespace caffe {

template <typename Dtype>
void PcaDivVarLayer<Dtype>::Reshape(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
  top[0]->ReshapeLike(*bottom[0]);
  //load pca mean blob
  const string& var_file = this->layer_param_.pca_mean_param().var_file();
  //std::cout << "mean_file = " << mean_file << std::endl;
  BlobProto blob_proto;
  ReadProtoFromBinaryFileOrDie(var_file.c_str(), &blob_proto);
  data_var_.FromProto(blob_proto);
  //std::cout << "data_mean = " << data_mean_.shape_string() << std::endl;
  //Dtype* data_mean = data_mean_.mutable_cpu_data();
  //for(int i = 0; i < 1472; i++)
  //   std::cout << *(data_mean + i) << std::endl;
}

template <typename Dtype>
void PcaDivVarLayer<Dtype>::Forward_cpu(const vector<Blob<Dtype>*>& bottom,
    const vector<Blob<Dtype>*>& top) {
  const Dtype* bottom_data = bottom[0]->cpu_data();
  Dtype* top_data = top[0]->mutable_cpu_data();
  Dtype* data_var = data_var_.mutable_cpu_data();
  //np.sqrt(pca.explained_variance_)
  caffe_powx<Dtype>(data_var_.count(), data_var, 0.5, data_var);
  int n = bottom[0]->num(); // n is batch_size
  int d = bottom[0]->count() / n;
  for (int i=0; i<n; ++i) { 
    caffe_div<Dtype>(d, bottom_data+i*d, data_var, top_data+i*d);
  } 
}

//PCA div var Just for deploy, not BP
template <typename Dtype>
void PcaDivVarLayer<Dtype>::Backward_cpu(const vector<Blob<Dtype>*>& top,
    const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom) {
	;
}


#ifdef CPU_ONLY
STUB_GPU(PcaDivVarLayer);
#endif

INSTANTIATE_CLASS(PcaDivVarLayer);
REGISTER_LAYER_CLASS(PcaDivVar);
}  // namespace caffe
