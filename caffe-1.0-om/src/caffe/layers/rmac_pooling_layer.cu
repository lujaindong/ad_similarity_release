#include <algorithm>
#include <cfloat>
#include <vector>

#include "caffe/layers/rmac_pooling_layer.hpp"
#include "caffe/util/math_functions.hpp"

namespace caffe {

template <typename Dtype>
__global__ void Max2dPoolForward(const int nthreads, 
    const int num, const int channels, int height, int width,
    const int hstart, const int hend, const int wstart, const int wend,
    const Dtype* const bottom_data, Dtype* const top_data) {
  CUDA_KERNEL_LOOP(index, nthreads) {
    const int pool_index = index;
    int n = pool_index / channels;
    int c = pool_index % channels;
    //printf("n = %d c= %d\n",n, c);
    for (int h = hstart; h <= hend; h++) {
      for (int w = wstart; w <= wend; w++) {
        const int i = ((n * channels + c) * height + h) * width + w;
        if(bottom_data[i] > top_data[pool_index]){
          top_data[pool_index] = bottom_data[i];
          }
        }
      }
    }
}

template <typename Dtype>
void l2_norm_gpu(const Blob<Dtype>* bottom, Blob<Dtype>* top, Blob<Dtype>& squared_, float eps){
    top->ReshapeLike(*bottom);
    squared_.ReshapeLike(*bottom);
    const Dtype* bottom_data = bottom->gpu_data();
    Dtype* top_data = top->mutable_gpu_data();
    Dtype* squared_data = squared_.mutable_gpu_data();
    int n = bottom->num();
    int d = bottom->count() / n;
    caffe_gpu_powx<Dtype>(n*d, bottom_data ,Dtype(2), squared_data);
    for (int i=0; i<n; ++i) {
      Dtype normsqr = 0.0;
      caffe_gpu_asum<Dtype>(d, squared_data+i*d, &normsqr);
      normsqr = pow(normsqr, 0.5) + eps;
      caffe_gpu_scale<Dtype>(d, 1.0/normsqr, bottom_data+i*d, top_data+i*d);
    }       
}


template <typename Dtype>
void RmacPoolingLayer<Dtype>::Forward_gpu(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
  float L = 3;
  float eps = pow(10, -6);
  Blob<Dtype>* x = bottom[0];
  float ovr = 0.4;
  int steps_array[6] = {2, 3, 4, 5, 6, 7};
  vector<float> steps(steps_array, steps_array+6);
  float W = x->shape()[3];
  float H = x->shape()[2];
  float w = min(W, H);
  float w2 = floor(w/2.0 - 1);
  //b is a vector
  vector<float> b(steps);
  for(int i = 0; i < steps.size(); i++){
    b[i] = (max(H,W) - w)/(steps[0]-1);
  }
  //steps(idx) regions for long dimension
  vector<float> t_1(b);
  for(int i = 0; i < b.size(); i++){
    t_1[i] = abs((w*w-w*b[i])/(w*w)-ovr);
  }
  float idx = 0;
  for(int i = 0; i < t_1.size(); i++){
    if(t_1[i] < t_1[idx])
      idx = i;
  }
  //region overplus per dimension
  float Wd = 0;
  float Hd = 0;
  if (H < W)
    Wd = idx;
  else if (H > W)
    Hd = idx;
  else
    ;
  //max_pool2d
  Blob<Dtype>* pool_out = &pool_out_;
  pool_out->Reshape(bottom[0]->num(), bottom[0]->channels(), 1, 1);
  const Dtype* bottom_data = bottom[0]->gpu_data();
  Dtype* pool_out_data = pool_out->mutable_gpu_data();
  int count = pool_out->count();
  caffe_gpu_set(pool_out->count(), Dtype(-FLT_MAX), pool_out_data);
  Max2dPoolForward<Dtype><<<CAFFE_GET_BLOCKS(count), CAFFE_CUDA_NUM_THREADS>>> (count, 
      bottom[0]->num(), bottom[0]->channels(), bottom[0]->height(), bottom[0]->width(),
      0, H-1, 0, W-1, 
      bottom_data, pool_out_data);

  Blob<Dtype>* pool_out_l2_norm = &pool_out_l2_norm_;
  l2_norm_gpu(pool_out, pool_out_l2_norm, squared_, eps);
  caffe_gpu_set(top[0]->count(), Dtype(0.0), top[0]->mutable_gpu_data());
  caffe_gpu_add(pool_out_l2_norm->count(), pool_out_l2_norm->gpu_data(), top[0]->mutable_gpu_data(), top[0]->mutable_gpu_data());
  int l = 1;
  for(; l < L+1; l++){
    float wl = floor(2*w/(l+1));
    float wl2 = floor(wl/2 - 1);
    float b = 0;
    if (l+Wd == 1)
      b = 0;
    else
      b = (W-wl)/(l+Wd-1);
    //center coordinates
    vector<float> cenW;
    cenW.resize(l-1+Wd+1);
    for(int i = 0; i < cenW.size(); i++){
      cenW[i] = floor(wl2 + i*b) - wl2;
    }
    if (l+Hd == 1)
      b = 0;
    else
      b = (H-wl)/(l+Hd-1);
    //center coordinates
    vector<float> cenH;
    cenH.resize(l-1+Hd+1);
    for(int i = 0; i < cenH.size(); i++){
      cenH[i] = floor(wl2 + i*b) - wl2;
    }
    for(int i = 0; i < cenH.size(); i++)
      for(int j = 0; j < cenW.size(); j++){
        float i_ = cenH[i];
        float j_ = cenW[j];
        if (wl == 0)
          continue;
        vector<int> w_list;
        w_list.resize(wl);
        for(int i = 0; i < w_list.size(); i++)
          w_list[i] = int(i_) + i;
        vector<int> h_list;
        h_list.resize(wl);
        for(int i = 0; i < h_list.size(); i++)
          h_list[i] = int(j_) + i;       
        count = pool_out->count();
        bottom_data = bottom[0]->gpu_data();
        pool_out_data = pool_out->mutable_gpu_data();
        pool_out_l2_norm = &pool_out_l2_norm_;
        caffe_gpu_set(pool_out->count(), Dtype(-FLT_MAX), pool_out_data);
        Max2dPoolForward<Dtype><<<CAFFE_GET_BLOCKS(count), CAFFE_CUDA_NUM_THREADS>>> (count, 
            bottom[0]->num(), bottom[0]->channels(), bottom[0]->height(), bottom[0]->width(),
            h_list[0], h_list[wl-1], w_list[0], w_list[wl-1], 
            bottom_data, pool_out_data);
        l2_norm_gpu(pool_out, pool_out_l2_norm, squared_, eps);
        caffe_gpu_add(pool_out_l2_norm->count(), pool_out_l2_norm->gpu_data(), top[0]->mutable_gpu_data(), top[0]->mutable_gpu_data());
      } 
  }
}

//rmac pooling just used for deploy, not need BP
template <typename Dtype>
void RmacPoolingLayer<Dtype>::Backward_gpu(const vector<Blob<Dtype>*>& top,
      const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom) {
	;
}
INSTANTIATE_LAYER_GPU_FUNCS(RmacPoolingLayer);
}// namespace caffe
