#include <algorithm>
#include <cfloat>
#include <vector>
#include <cmath>

#include "caffe/layers/rmac_pooling_layer.hpp"
#include "caffe/util/math_functions.hpp"

namespace caffe {

using std::min;
using std::max;

template <typename Dtype>
void RmacPoolingLayer<Dtype>::Reshape(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
  CHECK_EQ(4, bottom[0]->num_axes()) << "Input must have 4 axes, "
      << "corresponding to (num, channels, height, width)";
  int channels_ = bottom[0]->channels();
  //rmac pooling, the feature(H*W) will be (1*1)
  top[0]->Reshape(bottom[0]->num(), channels_, 1, 1);
}

//max_pool2d
template <typename Dtype>
void RmacPoolingLayer<Dtype>::max_pool2d(const Blob<Dtype>* bottom, Blob<Dtype>* top, 
      int hstart, int hend, int wstart, int wend) {
  int num = bottom->num();
  int channels = bottom->channels();
  int height = bottom->height();
  int width = bottom->width();
  top->Reshape(num, channels, 1, 1);
  Dtype* top_data = top->mutable_cpu_data();
  const Dtype* bottom_data = bottom->cpu_data();
  const int top_count = top->count();
  caffe_set(top_count, Dtype(-FLT_MAX), top_data);
  //int offset = ((n * channels() + c) * height() + h) * width() + w;
  for(int n = 0; n < num; n++){
    for(int c = 0; c < channels; c++){
      const int pool_index = ((n * channels + c) * 1 + 0) * 1 + 0;
      for(int h = hstart; h <= hend; h++){
        for(int w = wstart; w <= wend; w++){
          const int index = ((n * channels + c) * height + h) * width + w;
          if(bottom_data[index] > top_data[pool_index]){
            top_data[pool_index] = bottom_data[index];
          }
        }
      }
    }
  }
}

//l2_norm
template <typename Dtype>
void RmacPoolingLayer<Dtype>::l2_norm(const Blob<Dtype>* bottom, Blob<Dtype>* top, float eps) {
  top->ReshapeLike(*bottom);
  squared_.ReshapeLike(*bottom);
  const Dtype* bottom_data = bottom->cpu_data();
  Dtype* top_data = top->mutable_cpu_data();
  Dtype* squared_data = squared_.mutable_cpu_data();
  int n = bottom->num();
  int d = bottom->count() / n;
  caffe_sqr<Dtype>(n*d, bottom_data, squared_data);
  for (int i=0; i<n; ++i) {
    Dtype normsqr = pow(caffe_cpu_asum<Dtype>(d, squared_data+i*d), 0.5) + eps;
    caffe_cpu_scale<Dtype>(d, 1.0/normsqr, bottom_data+i*d, top_data+i*d);
  }  
} 

// rmac-pooling 
template <typename Dtype>
void RmacPoolingLayer<Dtype>::Forward_cpu(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
  float L = 3;
  float eps = pow(10, -6);
  Blob<Dtype>* x = bottom[0];
  float ovr = 0.4;
  int steps_array[6] = {2, 3, 4, 5, 6, 7};
  vector<float> steps(steps_array, steps_array+6);
  float W = x->shape()[3];
  float H = x->shape()[2];
  float w = min(W, H);
  float w2 = floor(w/2.0 - 1);
  //b is a vector
  vector<float> b(steps);
  for(int i = 0; i < steps.size(); i++){
    b[i] = (max(H,W) - w)/(steps[0]-1);
  }
  //steps(idx) regions for long dimension
  vector<float> t_1(b);
  for(int i = 0; i < b.size(); i++){
    t_1[i] = abs((w*w-w*b[i])/(w*w)-ovr);
  }
  float idx = 0;
  for(int i = 0; i < t_1.size(); i++){
    if(t_1[i] < t_1[idx])
      idx = i;
  }
  //region overplus per dimension
  float Wd = 0;
  float Hd = 0;
  if (H < W)
    Wd = idx;
  else if (H > W)
    Hd = idx;
  else
    ;
  Blob<Dtype>* pool_out = &pool_out_;
  Blob<Dtype>* pool_out_l2_norm = &pool_out_l2_norm_; 
  max_pool2d(bottom[0], pool_out, 0, H-1, 0, W-1);
  l2_norm(pool_out, pool_out_l2_norm, eps);
  caffe_copy(pool_out_l2_norm->count(), pool_out_l2_norm->cpu_data(), top[0]->mutable_cpu_data());
  int l = 1;
  for(; l < L+1; l++){
    //std::cout << "l= " << l << std::endl;
    float wl = floor(2*w/(l+1));
    float wl2 = floor(wl/2 - 1);
    float b = 0;
    if (l+Wd == 1)
      b = 0;
    else
      b = (W-wl)/(l+Wd-1);
    //center coordinates
    vector<float> cenW;
    cenW.resize(l-1+Wd+1);
    for(int i = 0; i < cenW.size(); i++){
      cenW[i] = floor(wl2 + i*b) - wl2;
    }
    if (l+Hd == 1)
      b = 0;
    else
      b = (H-wl)/(l+Hd-1);
    //center coordinates
    vector<float> cenH;
    cenH.resize(l-1+Hd+1);
    for(int i = 0; i < cenH.size(); i++){
      cenH[i] = floor(wl2 + i*b) - wl2;
    }
    for(int i = 0; i < cenH.size(); i++)
      for(int j = 0; j < cenW.size(); j++){
        float i_ = cenH[i];
        float j_ = cenW[j];
        if (wl == 0)
          continue;
        vector<int> w_list;
        w_list.resize(wl);
        for(int i = 0; i < w_list.size(); i++)
          w_list[i] = int(i_) + i;
        vector<int> h_list;
        h_list.resize(wl);
        for(int i = 0; i < h_list.size(); i++)
          h_list[i] = int(j_) + i;       
        max_pool2d(bottom[0], pool_out, h_list[0], h_list[wl-1],  w_list[0], w_list[wl-1]);
        l2_norm(pool_out, pool_out_l2_norm, eps);
        caffe_add(pool_out_l2_norm->count(), pool_out_l2_norm->cpu_data(), top[0]->mutable_cpu_data(), top[0]->mutable_cpu_data());
      } 
  }
}

//rmac pooling just used in delopy, not need backward
template <typename Dtype>
void RmacPoolingLayer<Dtype>::Backward_cpu(const vector<Blob<Dtype>*>& top,
      const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom) {
    ;
}


#ifdef CPU_ONLY
STUB_GPU(RmacPoolingLayer);
#endif

INSTANTIATE_CLASS(RmacPoolingLayer);
REGISTER_LAYER_CLASS(RmacPooling);
}  // namespace caffe
