#include <algorithm>
#include <cfloat>
#include <vector>
#include <iostream>

#include "thrust/device_vector.h"

#include "caffe/layers/pca_div_var_layer.hpp"
#include "caffe/util/math_functions.hpp"

namespace caffe {

template <typename Dtype>
void PcaDivVarLayer<Dtype>::Forward_gpu(const vector<Blob<Dtype>*>& bottom,
    const vector<Blob<Dtype>*>& top) {
  const Dtype* bottom_data = bottom[0]->gpu_data();
  Dtype* top_data = top[0]->mutable_gpu_data();
  Dtype* data_var = data_var_.mutable_gpu_data();
  caffe_gpu_powx<Dtype>(data_var_.count(), data_var, 0.5, data_var);
  int n = bottom[0]->num();
  int d = bottom[0]->count() / n;
  for (int i=0; i<n; ++i) {
    caffe_gpu_div<Dtype>(d, bottom_data+i*d, data_var, top_data+i*d);
  }
}

//PCA div var Just for deploy, not BP
template <typename Dtype>
void PcaDivVarLayer<Dtype>::Backward_gpu(const vector<Blob<Dtype>*>& top,
    const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom) {
	;
}

INSTANTIATE_LAYER_GPU_FUNCS(PcaDivVarLayer);


}  // namespace caffe
