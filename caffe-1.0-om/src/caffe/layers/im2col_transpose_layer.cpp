#include <vector>

#include "caffe/common.hpp"
#include "caffe/layer.hpp"
#include "caffe/util/im2col.hpp"
#include "caffe/layers/reverse_layer.hpp"

namespace caffe {

template <typename Dtype>
void Im2colTransposeLayer<Dtype>::LayerSetUp(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
  ConvolutionParameter conv_param = this->layer_param_.convolution_param();
   const int num_kernel_dims = conv_param.kernel_size_size();
  if (conv_param.has_kernel_h() || conv_param.has_kernel_w()) {
    kernel_h_ = conv_param.kernel_h();
    kernel_w_ = conv_param.kernel_w();
  }else{
    kernel_h_ = conv_param.kernel_size(0);
    kernel_w_ = conv_param.kernel_size((num_kernel_dims == 1) ? 0 : 1);
  }

  if (conv_param.has_pad_h() || conv_param.has_pad_w()) {
    pad_h_ = conv_param.pad_h();
    pad_w_ = conv_param.pad_w();
  } else {
    const int num_pad_dims = conv_param.pad_size();
    const int kDefaultPad = 0;
    pad_h_ = (num_pad_dims == 0) ? kDefaultPad :
          conv_param.pad(0);
    pad_w_ = (num_pad_dims == 0) ? kDefaultPad :
          conv_param.pad((num_pad_dims == 1) ? 0 : 1);
  }

  CHECK_GT(kernel_h_, 0) << "Filter dimensions cannot be zero.";
  CHECK_GT(kernel_w_, 0) << "Filter dimensions cannot be zero.";


  if (conv_param.has_stride_h() || conv_param.has_stride_w()) {
    stride_h_ = conv_param.stride_h();
    stride_w_ = conv_param.stride_w();
  } else {
    const int num_stride_dims = conv_param.stride_size();
    const int kDefaultStride = 1;
    stride_h_ = (num_stride_dims == 0) ? kDefaultStride :
          conv_param.stride(0);
    stride_w_ = (num_stride_dims == 0) ? kDefaultStride :
          conv_param.stride((num_stride_dims == 1) ? 0 : 1);
  }
}

template <typename Dtype>
void Im2colTransposeLayer<Dtype>::Reshape(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
  CHECK_EQ(4, bottom[0]->num_axes()) << "Input must have 4 axes, "
      << "corresponding to (num, channels, height, width)";
  channels_ = bottom[0]->channels();
  height_ = bottom[0]->height();
  width_ = bottom[0]->width();
  //top[0]->Reshape(
  //    bottom[0]->num(), channels_ * kernel_h_ * kernel_w_,
  //    (height_ + 2 * pad_h_ - kernel_h_) / stride_h_ + 1,
  //    (width_ + 2 * pad_w_ - kernel_w_) / stride_w_ + 1);
  
  // N x C x H x W -> W x H x N x C
  top[0]->Reshape(
      (width_ + 2 * pad_w_ - kernel_w_) / stride_w_ + 1,
      (height_ + 2 * pad_h_ - kernel_h_) / stride_h_ + 1,
      bottom[0]->num(),
      channels_ * kernel_h_ * kernel_w_);
  top[1]->Reshape(
      (width_ + 2 * pad_w_ - kernel_w_) / stride_w_ + 1,
      (height_ + 2 * pad_h_ - kernel_h_) / stride_h_ + 1,
      bottom[0]->num(),
      channels_ * kernel_h_ * kernel_w_);
}

template <typename Dtype>
void Im2colTransposeLayer<Dtype>::Forward_cpu(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
  const Dtype* bottom_data = bottom[0]->cpu_data();
  Dtype* top_data  = top[0]->mutable_cpu_data();
  Dtype* top_data1 = top[1]->mutable_cpu_data();
  for (int n = 0; n < bottom[0]->num(); ++n) {
    //im2col_cpu(bottom_data + bottom[0]->offset(n), channels_, height_,
    //im2col_transpose_cpu_new(bottom_data + bottom[0]->offset(n), channels_, height_,
    //    width_, kernel_h_, kernel_w_, pad_h_, pad_w_,
    //    stride_h_, stride_w_, top_data + top[0]->offset(n));
    im2col_transreverse_cpu(bottom_data + bottom[0]->offset(n), channels_, height_, width_,
    top_data + top[0]->offset(n), top_data1 + top[1]->offset(n));
  }
}

template <typename Dtype>
void Im2colTransposeLayer<Dtype>::Backward_cpu(const vector<Blob<Dtype>*>& top,
      const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom) {
  const Dtype* top_diff = top[0]->cpu_diff();
  Dtype* bottom_diff = bottom[0]->mutable_cpu_diff();
  //for (int n = 0; n < top[0]->num(); ++n) {
  for (int n = 0; n < top[0]->height(); ++n) {
    //col2im_cpu(top_diff + top[0]->offset(n), channels_, height_, width_,
    col2im_transpose_cpu_new(top_diff + top[0]->offset(n), channels_, height_, width_,
        kernel_h_, kernel_w_, pad_h_, pad_w_,
        stride_h_, stride_w_, bottom_diff + bottom[0]->offset(n));
  }
}

#ifdef CPU_ONLY
STUB_GPU(Im2colTransposeLayer);
#endif

INSTANTIATE_CLASS(Im2colTransposeLayer);
REGISTER_LAYER_CLASS(Im2colTranspose);

}  // namespace caffe
