#include <algorithm>
#include <vector>
#include <string>
#include <iostream>
#include "caffe/layers/pca_sub_mean_layer.hpp"
#include "caffe/util/math_functions.hpp"
#include "caffe/util/io.hpp"
namespace caffe {

template <typename Dtype>
void PcaSubMeanLayer<Dtype>::Reshape(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
  top[0]->ReshapeLike(*bottom[0]);
  //load pca mean blob
  const string& mean_file = this->layer_param_.pca_mean_param().mean_file();
  //std::cout << "mean_file = " << mean_file << std::endl;
  BlobProto blob_proto;
  ReadProtoFromBinaryFileOrDie(mean_file.c_str(), &blob_proto);
  data_mean_.FromProto(blob_proto);
  //std::cout << "data_mean = " << data_mean_.shape_string() << std::endl;
  //Dtype* data_mean = data_mean_.mutable_cpu_data();
  //for(int i = 0; i < 1472; i++)
  //   std::cout << *(data_mean + i) << std::endl;
}

template <typename Dtype>
void PcaSubMeanLayer<Dtype>::Forward_cpu(const vector<Blob<Dtype>*>& bottom,
    const vector<Blob<Dtype>*>& top) {
  const Dtype* bottom_data = bottom[0]->cpu_data();
  Dtype* top_data = top[0]->mutable_cpu_data();
  Dtype* data_mean = data_mean_.mutable_cpu_data();
  int n = bottom[0]->num(); // n is batch_size
  int d = bottom[0]->count() / n;
  for (int i=0; i<n; ++i) { 
    caffe_sub<Dtype>(d, bottom_data+i*d, data_mean, top_data+i*d);
  } 
}

//PCA sub mean Just for deploy, not BP
template <typename Dtype>
void PcaSubMeanLayer<Dtype>::Backward_cpu(const vector<Blob<Dtype>*>& top,
    const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom) {
	;
}


#ifdef CPU_ONLY
STUB_GPU(PcaSubMeanLayer);
#endif

INSTANTIATE_CLASS(PcaSubMeanLayer);
REGISTER_LAYER_CLASS(PcaSubMean);
}  // namespace caffe
