#ifdef USE_GLOG_GFLAG
#include <gflags/gflags.h>
#include <glog/logging.h>
#endif

#include <cstring>
#include <map>
#include <string>
#include <vector>
#include "boost/algorithm/string.hpp"
#include <boost/lexical_cast.hpp>
#include "caffe/caffe.hpp"
using caffe::Blob;
using caffe::Caffe;
using caffe::Net;
using caffe::Layer;
using caffe::Solver;
using caffe::shared_ptr;
using caffe::string;
using caffe::Timer;
using caffe::vector;
using std::ostringstream;
using namespace caffe;

#ifdef USE_GLOG_GFLAG
DEFINE_string(gpu, "-1",
              "Optional; run in GPU mode on given device ID");

DEFINE_string(model, "",
    "The model definition protocol buffer text file.");
class GLOGSupress
{
public:
	GLOGSupress(int supress_level = 5)
	{
		_restore_minloglevel = FLAGS_minloglevel;
		FLAGS_minloglevel = supress_level;
	}
	~GLOGSupress()
	{
		FLAGS_minloglevel = _restore_minloglevel;
	}
private:
	int _restore_minloglevel;
};
#else
class GLOGSupress
{
public:
    GLOGSupress(int level = 5){
    }
};
#endif



void gpu_idle(std::string model, int gpu_id){
    Caffe::SetDevice(gpu_id);
    Caffe::set_mode(Caffe::GPU);
    shared_ptr<Net<float> > net_;
    LOG(INFO) << model;
    {
        GLOGSupress supress(5);
        net_.reset(new Net<float>(model, TEST));
    }
    LOG(INFO) << "Using gpu: " << gpu_id;
    while(true){
        net_->Forward();
    }
}

int main(int argc, char** argv) {
    #if !USE_GLOG_GFLAG
        std::cout <<"Plese enable glog gflags";
    #else
        FLAGS_alsologtostderr = 1;
        caffe::GlobalInit(&argc, &argv);
        int gpu_id = boost::lexical_cast<int>(FLAGS_gpu);  
        gpu_idle(FLAGS_model, gpu_id);
    #endif
}
