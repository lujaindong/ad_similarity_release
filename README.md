# ad_similarity_release
# 素材指纹的发布工程
## caffe-1.0
1. 在caffe-1.0-om的基础上增加素材指纹需要的几个层.
2. makefile和makefile.config都是使用caffe-1.0-om的配置文件,不使用该配置文件可能会出错
3. src/caffe/api:为caffe库提供第三方接口,让外界调用forward函数
4. src/caffe/api/caffe_forward.cpp中实现具体的第三方接口
5. caffe_forward的预处理修改成,先缩放到[0,1],然后减均值[0.5, 0.5, 0.5],除方差[0.5, 0.5, 0.5],归一化到[-1,1]当中
6. 此caffe-1.0工程因为数据预处理比较特殊,一般的预处理只需要减均值[104,117,123].因此,此caffe-1.0工程不可用于其他项目的发布[数据预处理方式不同]

## ad_similarity_api
1. 素材指纹的发布工程,提供.so给后台进行调用
2. 素材指纹的配置文件vgg_extract_feat.prototxt中mean_file和var_file需要根据具体情况设置成绝对路径,不然可能会出现Segmentation fault的错误,主要是找不到两个文件
3. ad_similarity_api编译中CMakeLists.txt的3rdparty和c_api/CMakeLists.txt的3rdparty需要和caffe-1.0中的3rdparty保持一致,避免运行时问题

## ad_similarity_release的运行
1. 编译3rdparty
2. 设置3rdparty的路径,编译caffe-1.0
3. 将caffe-1.0/build/lib/libcaffe.a 拷贝到 ad_similarity_api/lib/中
4. 设置ad_similarity_api中的3rdparty
5. 修改配置文件vgg_extract_feat.prototxt中mean_file和var_file需要根据具体情况设置成绝对路径
6. make -j16
7. 修改python/AdSimilarity/中的3rdparty
8. make py 

## ad_similarity_api_support_ssd_detected_res
1. 支持从ssd的检测结果中提取bbox,抠图,然后将抠图进行素材指纹提取