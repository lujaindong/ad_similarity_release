#coding:utf-8
import sys, argparse, logging
import os
import glob
import numpy as np
from PIL import Image, ImageDraw 
import json
import time

root = '../'
sys.path.append(os.path.join(root, 'python'))
from collections import OrderedDict
from AdSimilarity import *
from multiprocessing import Pool
from collections import defaultdict

#模型配置文件
model_config = "../model/config.yml"
gpu_id = 0 
ad_similarity_classify = AdSimilarityClassify()
ad_similarity_classify.Init(model_config, gpu_id)

#names = ['../test_images/1.png', '../test_images/2.png']
names = ['../test_images/6882779219640860258_0.png', '../test_images/4862308141025407981_0.png']
image_buffer = strVector(len(names));
for idx, name in enumerate(names):
    fname = name
    image = np.asarray(Image.open(fname)) # cv2.imread(file)
    image_data = open(fname).read()
    image_buffer[idx] = image_data 
json_result = ad_similarity_classify.PredictFromImageDataJson(image_buffer, 0)
print(json_result)
json_result = json.loads(json_result)
hash_vec = json_result['hash_result']
for vec in hash_vec:
    lis_bit = [str(int(i)) for i in vec]
    str_bit = ''.join(lis_bit)
    hex_bit = hex(int(str_bit,2))
    print('hex_bit= ', hex_bit)
json_result = ad_similarity_classify.PredictFromImageDataJson(image_buffer, 1)
print(json_result)
