#coding:utf-8
import sys, argparse, logging
import os
import glob
import numpy as np
from PIL import Image, ImageDraw 
import json
import time
import caffe

root = '/data2/leojacklu/素材指纹/ad_similarity_api/'
sys.path.append(os.path.join(root, 'python'))
from collections import OrderedDict
from AdSimilarity import *
from multiprocessing import Pool
from collections import defaultdict

#模型配置文件
model_config = "/data2/leojacklu/素材指纹/ad_similarity_api/model/config.yml"
gpu_id = 1 
ad_similarity_classify = AdSimilarityClassify()
ad_similarity_classify.Init(model_config, gpu_id)

#for idx, file in enumerate(sorted(os.listdir(DIR))):
#    print ('idx=', idx, ' total=', len(os.listdir(DIR)))
#    fname = DIR + file
#CNT = 100
start = time.time()
DIR = '/data2/leojacklu/nsfw_project/nsfw_classify/data/nsfw_data_baiduyun_check/badcase数据/'
for idx, file in enumerate(sorted(os.listdir(DIR))):
    print ('idx=', idx, ' total=', len(os.listdir(DIR)))
    fname = DIR + file
#for idx in range(CNT):
    t1 = time.time()
    #fname = '../test_images/1.png' 
    image = np.asarray(Image.open(fname)) # cv2.imread(file)
    print '    Image Size:', image.shape
    if image.shape[0] == 0 or image.shape[1] == 0:
        sys.exit(0)
    image_data = open(fname).read()
    image_buffer = strVector(1);
    image_buffer[0] = image_data 
    json_result = ad_similarity_classify.PredictFromImageDataJson(image_buffer, 0)
    t2 = time.time()
    print('latency = ', 1000*(t2-t1))
    print(json_result)
end = time.time()
#latency = float(end-start)/CNT
latency = float(end-start)/len(os.listdir(DIR))
print(latency)

start = time.time()
DIR = '/data2/leojacklu/nsfw_project/nsfw_classify/data/nsfw_data_baiduyun_check/badcase数据/'
for idx, file in enumerate(sorted(os.listdir(DIR))):
    print ('idx=', idx, ' total=', len(os.listdir(DIR)))
    fname = DIR + file
#for idx in range(CNT):
    t1 = time.time()
    #fname = '../test_images/1.png' 
    image = np.asarray(Image.open(fname)) # cv2.imread(file)
    print '    Image Size:', image.shape
    if image.shape[0] == 0 or image.shape[1] == 0:
        sys.exit(0)
    image_data = open(fname).read()
    image_buffer = strVector(1);
    image_buffer[0] = image_data 
    json_result = ad_similarity_classify.PredictFromImageDataJson(image_buffer, 1)
    t2 = time.time()
    print('latency = ', 1000*(t2-t1))
    print(json_result)
end = time.time()
#latency = float(end-start)/CNT
latency = float(end-start)/len(os.listdir(DIR))
print(latency)
