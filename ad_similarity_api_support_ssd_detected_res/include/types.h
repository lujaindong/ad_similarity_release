/*
Copyright 2017 colinhli
*/
#pragma once
#include <vector>
#include <string>

typedef struct TPoint2f {
    TPoint2f()
        : x(0), y(0) {
    }
    TPoint2f(float p_x, float p_y)
        : x(p_x), y(p_y) {
    }
    float x;
    float y;
} TPoint2f;

typedef struct RotateRect {
    RotateRect()
        : width(0), height(0), rotate(0) {
    }
    RotateRect(float x1, float y1, float x2, float y2) {
        center.x = (x2 + x1) / 2;
        center.y = (y2 + y1) / 2;
        width = x2 - x1;
        height = y2 - y1;
        rotate = 0;
    }
    std::vector<TPoint2f> Points() const {
        std::vector<TPoint2f> points;
        float x1, y1, x2, y2;
        x1 = center.x - width / 2;
        y1 = center.y - height / 2;
        x2 = center.x + width / 2;
        y2 = center.y + height / 2;

        points.push_back(TPoint2f(x1, y1));
        points.push_back(TPoint2f(x2, y1));
        points.push_back(TPoint2f(x2, y2));
        points.push_back(TPoint2f(x1, y2));
        return points;
    }
    TPoint2f center;
    float width;
    float height;
    float rotate;
} RotateRect;

typedef struct DetBox {
    DetBox()
        : conf(0), type(0) {
    }
    RotateRect rect;
    float conf;
    int type;
} DetBox;

typedef struct BoxItem {
    DetBox box;
    std::string text;
} BoxItem;

typedef struct NSFWItem {
    NSFWItem()
        : nsfw_type(-1), nsfw_conf(-1) {
    }
    DetBox box;
    int nsfw_type;
    float nsfw_conf;
} NSFWItem;

typedef struct BaseResult {
    int image_width;
    int image_height;
    int errorcode;
    std::string errormsg;
    int latency;
    BaseResult() {
        image_width = 0;
        image_height = 0;
        latency = 0;
        errorcode = 0;
    }
} BaseResult;

typedef struct DetectorResult {
    std::vector<BoxItem> Items;
    BaseResult base;
} DetectorResult;

typedef DetectorResult BodyResult;

typedef std::pair<int, float> NSFWPrediction;

typedef struct NSFWResult {
    NSFWResult()
        : image_width(0), image_height(0) {
    }
    std::vector<NSFWItem> Items;
    int image_width;
    int image_height;
    std::string error;
} NSFWResult;

typedef std::pair<int, float> Prediction;
typedef struct CNNClassifyResult {
    CNNClassifyResult(){
    }
    BaseResult base;
    Prediction predict;
} CNNClassifyResult;

typedef struct AdSimilarityResult {
    AdSimilarityResult(){
    }
    BaseResult base;
    std::vector<std::vector<float>> float_predicts;
    std::vector<std::vector<int>> hash_predicts;
    std::vector<std::string> hex_bit_vec;
} AdSimilarityResult;

enum ReturnCode {
    kSuccess = 0,
    kOpenCVError = -1
};
