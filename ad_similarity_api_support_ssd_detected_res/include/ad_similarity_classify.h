#pragma once
#include <opencv2/opencv.hpp>
#include <string>
#include <vector>
#include <types.h>
namespace OM {
struct AdSimilarityPrivate;
class AdSimilarityClassify {
  public:
    AdSimilarityClassify();
    virtual ~AdSimilarityClassify();
    virtual int Init(std::string model_config, int  gpu_id);
    virtual std::vector<std::vector<float>> Predict(const std::vector<cv::Mat> &image, int type);
    virtual AdSimilarityResult PredictFromImageData(const std::vector<std::string> &image_buffer, int type);
    virtual std::string PredictFromImageDataJson(const std::vector<std::string> &image_buffer, int type);
    virtual std::string PredictFromDetectedRes(const std::string &image_string, const std::string &ssd_res, int type, int batch_size);
    virtual std::vector<std::string> PredictFromDetectedResDataJson(const std::vector<std::string> &image_buffer, const std::vector<std::string> &detected_res, int type, int batch_size);
  private:
    AdSimilarityPrivate *priv;
};
}

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _AdSimilarityEngine {
    intptr_t p_handle;
} AdSimilarityEngine;

AdSimilarityEngine *CreateAdSimilarityEngine(std::string model_config, int  gpu_id);
void ReleaseAdSimilarityEngine(AdSimilarityEngine *p_engine);
//type = 0 代表提取哈希特征[0,1,1..]
//type = 1 代表提取浮点特征[0.1, 0.2, ..]
//传进去的是一个image_buffer list
std::string AdSimilarityEnginePredict(AdSimilarityEngine *p_engine, const std::vector<std::string> &images_buffer, int type);
std::vector<std::string> AdSimilarityEnginePredictFromDetectedRes(AdSimilarityEngine *p_engine, const std::vector<std::string> &images_buffer, const std::vector<std::string> &detected_res, int type, int batch_size);
#ifdef __cplusplus
}
#endif
