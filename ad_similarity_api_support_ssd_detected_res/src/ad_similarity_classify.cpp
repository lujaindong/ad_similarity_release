#include "ad_similarity_classify.h"
#include "api/caffe_forward.h"
#include "api/blob_data.h"
#include "caffe_model_config.h"
#include <opencv2/opencv.hpp>
#include <memory>
#include <json/json.h>
#include <boost/date_time/posix_time/posix_time.hpp>
#include "json_serialize.h"
#include <boost/filesystem.hpp>
#include <sstream>
#include <string>
#include <iomanip>
#include <iostream>
#include <bitset> 
using caffe::CaffeForward;
using caffe::BlobData;

namespace OM {
typedef struct AdSimilarityPrivate {
    std::unique_ptr<CaffeForward> classify_net;
    std::unique_ptr<CaffeModelConfig> config;
} AdSimilarityPrivate;

//将int转成16进制字符串
std::string dec2hex(unsigned long long i) {
    std::stringstream ioss; //定义字符串流
    std::string s_temp; //存放转化后字符
    //ioss << setiosflags(std::ios::uppercase) << std::hex << i; //以十六制(大写)形式输出
    ioss << resetiosflags(std::ios::uppercase) << std::hex << i; //以十六制(小写)形式输出//取消大写的设置
    ioss >> s_temp;
    std::string t = "0x";
    return t+s_temp;
}

AdSimilarityClassify::AdSimilarityClassify() {
    priv = new AdSimilarityPrivate();
}

AdSimilarityClassify::~AdSimilarityClassify() {
    delete priv;
}

int AdSimilarityClassify::Init(std::string model_config, int gpu_id) {
    priv->config.reset(new CaffeModelConfig(model_config));
    priv->classify_net.reset(new CaffeForward(priv->config->model_file, priv->config->weights_file, priv->config->mean_value, priv->config->var_value, gpu_id));
    return 0;
}

std::vector<std::vector<float>> AdSimilarityClassify::Predict(const std::vector<cv::Mat> &images, int type) {
    std::map<string, std::shared_ptr<BlobData>> predicts;
    priv->classify_net->Forward(images);
    //std::cout << "success"<< std::endl; 
    std::string blob_name = "";
    if (type == 0){
        blob_name = "pca_itq_output";
    }
    else if (type == 1){
        blob_name = "output_l2_norm";
    }
    std::vector<std::string> names;
    names.push_back(blob_name);
    predicts = priv->classify_net->GetBlobs(names);
    std::shared_ptr<BlobData> blob_ptr = predicts[blob_name];
    //shape
    //std::vector<int> shape = blob_ptr->shape();
    //std::ostringstream stream;
    //for (int i = 0; i < shape.size(); ++i) {
    //      stream << shape[i] << " ";
    //}
    //std::cout << stream.str() << std::endl;  
    //shape
    
    //get result
    std::vector<std::vector<float>> result;
    int num = blob_ptr->num();
    for(int i=0; i<num; i++){
        const float* begin = blob_ptr->data_pointer() + blob_ptr->offset(i);
        const float* end = blob_ptr->data_pointer() + blob_ptr->offset(i+1);
        result.push_back(std::vector<float>(begin, end));
    }
    if (type == 0){
        for(int i = 0; i < result.size(); i ++ ){
            for(int j = 0; j < result[i].size(); j ++){
                if (result[i][j] < 0) {
                    result[i][j] = 0;
                }else{
                    result[i][j] = 1;
                }
            }
        }
    }   
    return result;
}

 
AdSimilarityResult AdSimilarityClassify::PredictFromImageData(const std::vector<std::string> &data_buffer, int type) {
    AdSimilarityResult result;
    try {
        boost::posix_time::ptime start_cpu = boost::posix_time::microsec_clock::local_time();
        std::vector<cv::Mat> images;
        for(int i = 0; i < data_buffer.size(); i++){ 
            std::vector<uchar> image_data(data_buffer[i].data(),
                                      data_buffer[i].data() + data_buffer[i].size());
            cv::Mat image = cv::imdecode(image_data, CV_LOAD_IMAGE_ANYCOLOR);
            images.push_back(image);
        }
        //std::cout << images.size() << std::endl;
        if (type == 0){
            std::vector<std::vector<float>> hash_predict = Predict(images, type);
            std::vector<std::vector<int>> hash_predict_int;
            hash_predict_int.resize(hash_predict.size());
            for(int i = 0; i < hash_predict.size(); i++)
                hash_predict_int[i].resize(hash_predict[i].size());
            for(int i = 0; i < hash_predict.size(); i ++ ){
                for(int j = 0; j <hash_predict[i].size(); j ++){
                    hash_predict_int[i][j] = int(hash_predict[i][j]);
                }
            }
            result.hash_predicts = hash_predict_int;
            std::vector<std::string> hex_bit_vec;
            for(int i = 0; i < hash_predict_int.size(); i++){
                int size = hash_predict_int[i].size();
                std::string total = "";
                for(int j = 0; j < size; j++){
                    std::string tmp = std::to_string(hash_predict_int[i][j]);
                    total += tmp;
                }
                //std::cout << "total = " << std::bitset<64>(total).to_ullong() << std::endl;
                //选用的是64位hash特征,bitset需要申请64位空间
                unsigned long long num = std::bitset<64>(total).to_ullong();
                std::string res = dec2hex(num);
                hex_bit_vec.push_back(res);
            }
            result.hex_bit_vec = hex_bit_vec;
        }else if(type == 1){
            std::vector<std::vector<float>> float_predict = Predict(images, type);
            result.float_predicts = float_predict;
        }
        result.base.image_width = -1;
        result.base.image_height = -1;
        boost::posix_time::ptime stop_cpu = boost::posix_time::microsec_clock::local_time();
        float time_span = (stop_cpu - start_cpu).total_microseconds() / 1000;
        result.base.latency = time_span;
    } catch (cv::Exception &e) {
        result.base.errormsg = e.err + " in " + e.func;
    }

    return result;
}

std::string AdSimilarityClassify::PredictFromDetectedRes(const std::string &image_string, const std::string &ssd_res, int type, int batch_size){
    std::string result;
    try {
        boost::posix_time::ptime start_cpu = boost::posix_time::microsec_clock::local_time();
        //get bbox from json
        Json::Reader reader;
        Json::Value root;
        bool parsingSuccessful = reader.parse(ssd_res, root);
        if (!parsingSuccessful){
            std::cout << "Error parsing the string" << std::endl;
        }
        Json::Value regions = root["data"]["regions"];
        std::vector<std::vector<int>> bboxs;
        for(int i = 0; i < regions.size(); i++){
            std::vector<int> bbox;
            for(int j = 0; j < regions[i]["bbox"].size(); j++){
                bbox.push_back(regions[i]["bbox"][j].asInt());
            }
            bboxs.push_back(bbox);
        }
        //std::cout << "bbox num = " << bboxs.size() << std::endl;

        //decode image from image string
        std::vector<uchar> image_data(image_string.data(), image_string.data() + image_string.size());
        cv::Mat image = cv::imdecode(image_data, CV_LOAD_IMAGE_ANYCOLOR);
        //std::cout << "image shape = " << image.rows << " " << image.cols << std::endl; 
        //crop image use bbox
        std::vector<cv::Mat> sub_images;
        for(int i = 0; i < bboxs.size(); i++){
            std::vector<int> x_s;
            std::vector<int> y_s;
            for(int j = 0; j < bboxs[i].size(); j++){
                if (j%2 ==0)
                    x_s.push_back(bboxs[i][j]);
                else
                    y_s.push_back(bboxs[i][j]);
            }
            int x_min = *min_element(x_s.begin(), x_s.end());
            int x_max = *max_element(x_s.begin(), x_s.end());
            int y_min = *min_element(y_s.begin(), y_s.end());
            int y_max = *max_element(y_s.begin(), y_s.end());        
            cv::Rect roi_rect;
            x_min = x_min < 0 ? 0 : x_min;
            y_min = y_min < 0 ? 0 : y_min;
            roi_rect.x = x_min;
            roi_rect.y = y_min;   
            roi_rect.width = x_max - x_min + 1;
            if(roi_rect.width > image.cols || x_min < 0 || x_max > image.cols){
                std::cout << "crop bbox is wrong, roi_rect.width > image.cols or x_min < 0 or x_max > image.cols" << std::endl;
            }
            roi_rect.height = y_max - y_min + 1;
            if(roi_rect.height > image.rows || y_min < 0 || y_max > image.rows){
                std::cout << "crop bbox is wrong, roi_rect.height > image.rows or y_min < 0 or y_max > image.rows" << std::endl;
            }
            cv::Mat sub_image = image(roi_rect).clone();
            //std::cout << "sub_image shape = " << sub_image.rows << " " << sub_image.cols << std::endl; 
            sub_images.push_back(sub_image);
        }
        //std::cout << "sub image num = " << sub_images.size() << std::endl;

        if (type == 0){
            std::vector<std::vector<float>> hash_predict;
            std::vector<cv::Mat> batch_images;
            int num = 0;
            for(int i = 0; i < sub_images.size(); i++){
                if (num != batch_size){
                    batch_images.push_back(sub_images[i]);
                    num += 1;
                }else{
                    num = 0; 
                    std::vector<std::vector<float>> batch_predict_float = Predict(batch_images, type);  
                    for(int i = 0; i < batch_predict_float.size(); i++){
                        hash_predict.push_back(batch_predict_float[i]);
                    }
                    batch_images.clear();
                    //std::cout << "batch size = " << batch_predict_float.size() << std::endl;
                    batch_images.push_back(sub_images[i]);
                }
            }
            if (batch_images.size() > 0){
                std::vector<std::vector<float>> batch_predict_float = Predict(batch_images, type);
                for(int i = 0; i < batch_predict_float.size(); i++){
                    hash_predict.push_back(batch_predict_float[i]);
                }
                //std::cout << "batch size xx = " << batch_predict_float.size() << std::endl;
            }
            std::vector<std::vector<int>> hash_predict_int;
            hash_predict_int.resize(hash_predict.size());
            for(int i = 0; i < hash_predict.size(); i++)
                hash_predict_int[i].resize(hash_predict[i].size());
            for(int i = 0; i < hash_predict.size(); i ++ ){
                for(int j = 0; j <hash_predict[i].size(); j ++){
                    hash_predict_int[i][j] = int(hash_predict[i][j]);
                }
            }
            //add res_hash
            for(int i = 0; i < hash_predict_int.size(); i++){
                    for(int j = 0; j < hash_predict_int[i].size(); j++){
                        root["data"]["regions"][i]["res_hash"].append(hash_predict_int[i][j]);
                    }
            }
            std::vector<std::string> hex_bit_vec;
            for(int i = 0; i < hash_predict_int.size(); i++){
                int size = hash_predict_int[i].size();
                std::string total = "";
                for(int j = 0; j < size; j++){
                    std::string tmp = std::to_string(hash_predict_int[i][j]);
                    total += tmp;
                }
                //std::cout << "total = " << std::bitset<64>(total).to_ullong() << std::endl;
                //选用的是64位hash特征,bitset需要申请64位空间
                unsigned long long num = std::bitset<64>(total).to_ullong();
                std::string res = dec2hex(num);
                hex_bit_vec.push_back(res);
            }
            //add hex_bit
            for(int i = 0; i < hex_bit_vec.size(); i++){
                 root["data"]["regions"][i]["hex_bit"] = hex_bit_vec[i];
            }
            //set null for res_float
            for(int i = 0; i < hex_bit_vec.size(); i++){
                 root["data"]["regions"][i]["res_float"] = "";
            }            
        }else if(type == 1){
            std::vector<std::vector<float>> float_predict;
            std::vector<cv::Mat> batch_images;
            int num = 0;
            for(int i = 0; i < sub_images.size(); i++){
                if (num != batch_size){
                    batch_images.push_back(sub_images[i]);
                    num += 1;
                }else{
                    num = 0;
                    std::vector<std::vector<float>> batch_predict_float = Predict(batch_images, type);
                    //std::cout << "batch size = " << batch_predict_float.size() << std::endl;
                    for(int i = 0; i < batch_predict_float.size(); i++){
                        float_predict.push_back(batch_predict_float[i]);
                    }
                    batch_images.clear();
                    batch_images.push_back(sub_images[i]);
                }
            }
            if (batch_images.size() > 0){
                std::vector<std::vector<float>> batch_predict_float = Predict(batch_images, type);
                for(int i = 0; i < batch_predict_float.size(); i++){
                    float_predict.push_back(batch_predict_float[i]);
                }
                //std::cout << "batch size xx = " << batch_predict_float.size() << std::endl;
            }
            //std::vector<std::vector<float>> float_predict = Predict(sub_images, type);
            //set null for hex_bit and res_hash
            for(int i = 0; i < float_predict.size(); i++){
                 root["data"]["regions"][i]["res_hash"] = "";
                 root["data"]["regions"][i]["hex_bit"] = "";
            } 
            //add res_float
            for(int i = 0; i < float_predict.size(); i++){
                    for(int j = 0; j < float_predict[i].size(); j++){
                        root["data"]["regions"][i]["res_float"].append(float_predict[i][j]);
                    }
            }   
        }
     
        boost::posix_time::ptime stop_cpu = boost::posix_time::microsec_clock::local_time();
        float time_span = (stop_cpu - start_cpu).total_microseconds() / 1000;
        root["data"]["latency"] = time_span;
        Json::StreamWriterBuilder wbuilder;
        wbuilder["commentStyle"] = "None";
        wbuilder["indentation"] = "";
        result = Json::writeString(wbuilder, root);
    } catch (cv::Exception &e) {
        result  = e.err + " in " + e.func;
    }

    return result;
}

//输入image string list 和 ssd的检测结果list
//type = 0, 提取hash特征, type = 1, 提取float特征
//将ssd的图片进行裁剪图片,然后进行素材指纹提取，然后回传
std::vector<std::string> AdSimilarityClassify::PredictFromDetectedResDataJson(const std::vector<std::string> &image_buffer, const std::vector<std::string> &detected_res, int type, int batch_size) {
    if (image_buffer.size() != detected_res.size()) {
        std::cout << "images num must equal to the detected_res num" << std::endl;
    }
    std::vector<std::string> result;
    for(int i = 0; i < image_buffer.size(); i++){
        std::string one_res = PredictFromDetectedRes(image_buffer[i], detected_res[i], type, batch_size);
        result.push_back(one_res);
    }
    return result;
}

std::string AdSimilarityClassify::PredictFromImageDataJson(const std::vector<std::string> &data_buffer, int type) {
    AdSimilarityResult result;
    result = PredictFromImageData(data_buffer, type);
    return AdSimilarityResult2Json(result);
}
} // namespace OM

AdSimilarityEngine *CreateAdSimilarityEngine(std::string model_config, int  gpu_id) {
    AdSimilarityEngine *engine = new AdSimilarityEngine();
    engine->p_handle = 0;
    std::unique_ptr<OM::AdSimilarityClassify> ad_similarity_engine(new OM::AdSimilarityClassify());
    try {
        ad_similarity_engine->Init(model_config, gpu_id);
        engine->p_handle = (intptr_t)ad_similarity_engine.release();
    } catch (...) {
        std::cout << "ad similarity model init error" << std::endl;
        engine->p_handle = 0;
    }
    return engine;
}

void ReleaseAdSimilarityEngine(AdSimilarityEngine *p_engine) {
    if (p_engine != nullptr) {
        std::unique_ptr<OM::AdSimilarityClassify> logo_engine((OM::AdSimilarityClassify *)(p_engine->p_handle));
        p_engine->p_handle = 0;
        delete p_engine;
    }
}

std::string AdSimilarityEnginePredict(AdSimilarityEngine *p_engine, const std::vector<std::string> &images_buffer, int type) {
    AdSimilarityResult result;
    OM::AdSimilarityClassify *ad_similarity_engine = (OM::AdSimilarityClassify *)(p_engine->p_handle);
    if (ad_similarity_engine == nullptr) {
        result.base.errormsg = "engine not initialized";
        result.base.errorcode = -1;
    } else {
        result = ad_similarity_engine->PredictFromImageData(images_buffer, type);
    }
    return AdSimilarityResult2Json(result);
}

std::vector<std::string> AdSimilarityEnginePredictFromDetectedRes(AdSimilarityEngine *p_engine, const std::vector<std::string> &images_buffer, const std::vector<std::string> &detected_res, int type, int batch_size) {
    std::vector<std::string> result;
    OM::AdSimilarityClassify *ad_similarity_engine = (OM::AdSimilarityClassify *)(p_engine->p_handle);
    if (ad_similarity_engine == nullptr) {
        std::cout << "engine not initialized" << std::endl;
    } else {
        result = ad_similarity_engine->PredictFromDetectedResDataJson(images_buffer, detected_res, type, batch_size);
    }
    return result;
}
