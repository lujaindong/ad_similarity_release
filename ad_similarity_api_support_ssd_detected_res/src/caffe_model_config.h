#ifndef SRC_CAFFE_MODEL_CONFIG_H
#define SRC_CAFFE_MODEL_CONFIG_H
#include <string>
#include <vector>

namespace OM {
typedef struct CaffeModelConfig {
    CaffeModelConfig(std::string model_dir);
    std::string model_file;
    std::string weights_file;
    std::string mean_value;
    std::string var_value;
} CaffeModelConfig;
}
#endif  //  SRC_CAFFE_MODEL_CONFIG_H
