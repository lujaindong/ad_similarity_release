#pragma once
#include "blob_data.h"
#include <opencv2/opencv.hpp>
#include <memory>
#include <vector>
#include <map>
#include <string>
struct CaffeForwardPrivate;
using std::string;
namespace caffe {
class CaffeForward {
  public:
    CaffeForward(const std::string &model_file,
                 const std::string &weights_file,
                 const std::string &mean_value, 
                 const std::string &var_value, int gpu);
    CaffeForward();
    ~CaffeForward();
    std::map<string, std::shared_ptr<BlobData>> Forward(std::vector<cv::Mat> images);
    std::map<string, std::shared_ptr<BlobData>> GetBlobs(std::vector<std::string> names);
    std::shared_ptr<CaffeForward> cloneShared();
    cv::Size GetInputSize();
    void SetInputSize(cv::Size size);
    int Channels();

  protected:
    void SetMean(const string &mean_value);
    void SetVar(const string &var_value);
    void WrapInputLayer(std::vector<cv::Mat> *input_channels);
    void Preprocess(std::vector<cv::Mat> images, std::vector<cv::Mat> *input_channels);
    void Preprocess_step(cv::Mat &img, std::vector<cv::Mat> *input_channels, int step);

  protected:
    CaffeForwardPrivate *priv;
};

} // namespace caffe
