#include <iostream>
#include <string>
#include <dlfcn.h>
#include <fstream>
#include <vector>
// #include <gflags/gflags.h>
typedef struct _AdSimilarityEngine {
    intptr_t p_handle;
} AdSimilarityEngine;

typedef AdSimilarityEngine *(*CreateAdSimilarityEngine)(std::string model_config, int  gpu_id);
typedef std::string (*AdSimilarityEnginePredict)(AdSimilarityEngine *p_engine, const std::vector<std::string> &image_buffer, int type);
typedef std::vector<std::string> (*AdSimilarityEnginePredictFromDetectedRes)(AdSimilarityEngine *p_engine, const std::vector<std::string> &images_buffer, const std::vector<std::string> &detected_res, int type, int batch_size);
typedef void (*ReleaseAdSimilarityEngine)(AdSimilarityEngine *p_engine);

CreateAdSimilarityEngine F_CREATE_AD_SIMILARITY_ENGINE;
AdSimilarityEnginePredict F_AD_SIMILARITY_PREDICT;
AdSimilarityEnginePredictFromDetectedRes F_AD_SIMILARITY_PREDICT_FROM_SSD_DETECTED_RES;
ReleaseAdSimilarityEngine F_RELEASE_AD_SIMILARITY_ENGINE;
AdSimilarityEngine *G_AD_SIMILARITY_ENGINE;


int main(int argc, char **argv) {
    // ::google::ParseCommandLineFlags(&argc, &argv, true);
    std::string so_path = argv[1];
    std::cout << "Begin loading:" << so_path << std::endl;
    void *handle = dlopen(so_path.c_str(), RTLD_LAZY);
    if (!handle) {
        std::cout << "Cannot open library:" << dlerror() << std::endl;
        return 1;
    }
    std::cout << "Load success" << std::endl;
    
    std::cout << "Query CreateAdSimilarityEngine" << std::endl;
    F_CREATE_AD_SIMILARITY_ENGINE = (CreateAdSimilarityEngine) dlsym(handle, "CreateAdSimilarityEngine");
    if (dlerror()) {
        std::cout << "Cannot load symbol 'CreateAdSimilarityEngine'" << dlerror();
        dlclose(handle);
        return 1;
    }

    std::cout << "Query AdSimilarityEnginePredictFromDetectedRes" << std::endl;
    F_AD_SIMILARITY_PREDICT_FROM_SSD_DETECTED_RES = (AdSimilarityEnginePredictFromDetectedRes) dlsym(handle, "AdSimilarityEnginePredictFromDetectedRes");
    if (dlerror()) {
        std::cout << "Cannot load symbol 'AdSimilarityEnginePredict'" << dlerror();
        dlclose(handle);
        return 1;
    }

    std::cout << "Query ReleaseAdSimilarityEngine" << std::endl;
    F_RELEASE_AD_SIMILARITY_ENGINE = (ReleaseAdSimilarityEngine) dlsym(handle, "ReleaseAdSimilarityEngine");
    if (dlerror()) {
        std::cout << "Cannot load symbol 'ReleaseAdSimilarityEngine'" << dlerror();
        dlclose(handle);
        return 1;
    }

    // call CreateAdSimilarityEngine
    std::string ad_similarity_model = argv[2];
    int gpu_id = atoi(argv[4]);

    std::cout << "Call CreateAdSimilarityEngine" << std::endl;
    G_AD_SIMILARITY_ENGINE = F_CREATE_AD_SIMILARITY_ENGINE(ad_similarity_model, gpu_id);
    if (!G_AD_SIMILARITY_ENGINE) {
        std::cout <<  "Call 'CreateAdSimilarityEngine' fail: " << dlerror();
        return 1;
    }

    std::cout << "Call AdSimilarityEnginePredict" << std::endl;
    //std::string file_name = "./6e0ce0fb-f53e-45bc-8019-199a1427f92e.jpg";
    //std::string file_name = "./0.jpg";
    std::string file_name = "./6882779219640860258_0.png";
    //std::string file_name = "./4862308141025407981_0.png";
    std::ifstream ifs(file_name);
    std::string image_buffer((std::istreambuf_iterator<char>(ifs)),
                             (std::istreambuf_iterator<char>()));
    std::vector<std::string> data_buffer;
    data_buffer.push_back(image_buffer);
    std::vector<std::string> ssd_res;
    //std::string text = "{\"data\":{\"errorcode\":0,\"errormsg\":\"\",\"image_height\":1390,\"image_width\":1100,\"latency\":169,\"regions\":[{\"bbox\":[351,335,789,335,789,1314,351,1314],\"confidence\":0.59434276819229126,\"name\":\"T\\u6064\",\"type\":18},{\"bbox\":[346,342,798,342,798,1377,346,1377],\"confidence\":0.15223097801208496,\"name\":\"\\u886c\\u886b\",\"type\":3},{\"bbox\":[348,329,785,329,785,1337,348,1337],\"confidence\":0.1284075528383255,\"name\":\"\\u7f8a\\u6bdb\\u8863\",\"type\":6}]}}";
    //std::string text = "{\"data\":{\"errorcode\":0,\"errormsg\":\"\",\"image_height\":870,\"image_width\":690,\"latency\":170,\"regions\":null}}";
    //6882779219640860258_0.png string
    std::string text = "{\"data\":{\"errorcode\":0,\"errormsg\":\"\",\"image_height\":560,\"image_width\":1000,\"latency\":169,\"regions\":[{\"bbox\":[100,150,449,150,449,549,100,549],\"confidence\":0.59434276819229126,\"name\":\"T\\u6064\",\"type\":18},{\"bbox\":[0,0,999,0,999,559,0,559],\"confidence\":0.15223097801208496,\"name\":\"\\u886c\\u886b\",\"type\":3},{\"bbox\":[0,0,999,0,999,559,0,559],\"confidence\":0.1284075528383255,\"name\":\"\\u7f8a\\u6bdb\\u8863\",\"type\":6}]}}";
    //std::string text = "{\"data\":{\"errorcode\":0,\"errormsg\":\"\",\"image_height\":334,\"image_width\":960,\"latency\":169,\"regions\":[{\"bbox\":[0,0,959,0,959,333,0,333],\"confidence\":0.59434276819229126,\"name\":\"T\\u6064\",\"type\":18},{\"bbox\":[0,0,959,0,959,333,0,333],\"confidence\":0.15223097801208496,\"name\":\"\\u886c\\u886b\",\"type\":3},{\"bbox\":[0,0,959,0,959,333,0,333],\"confidence\":0.1284075528383255,\"name\":\"\\u7f8a\\u6bdb\\u8863\",\"type\":6}]}}";
    ssd_res.push_back(text);
    //每次计算2张图片
    int batch_size = 2;
    //type = 0:提取哈希特征
    std::vector<std::string> json_result_hash = F_AD_SIMILARITY_PREDICT_FROM_SSD_DETECTED_RES(G_AD_SIMILARITY_ENGINE, data_buffer, ssd_res, 0, batch_size);
    std::cout << "ad similarity hash Result:" << std::endl;
    for(int i = 0; i < json_result_hash.size(); i++){
        std::cout << json_result_hash[i] << std::endl;
    }
    //type = 1:提取浮点数特征
    std::vector<std::string> json_result_float = F_AD_SIMILARITY_PREDICT_FROM_SSD_DETECTED_RES(G_AD_SIMILARITY_ENGINE, data_buffer, ssd_res, 1, batch_size);
    std::cout << "ad similarity float Result:" << std::endl;
    for(int i = 0; i < json_result_float.size(); i++){
        std::cout << json_result_float[i] << std::endl;
    } 
    std::cout << "Call ReleaseAdSimilarityEngine" << std::endl;

    F_RELEASE_AD_SIMILARITY_ENGINE(G_AD_SIMILARITY_ENGINE);

    std::cout << "Done" << std::endl;

    return 0;
}
