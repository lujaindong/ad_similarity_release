cmake_minimum_required (VERSION 2.8)
project (nsfw_c_api)
#set(CMAKE_CXX_COMPILER /data/gcc-4.9.4/bin/g++)

set(CMAKE_SKIP_BUILD_RPATH FALSE)
set(CMAKE_BUILD_WITH_INSTALL_RPATH FALSE)
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

set(CMAKE_INSTALL_PREFIX ${PROJECT_SOURCE_DIR}/../)

set(3rdparty /data/3rdparty/)
#set(OpenCV_DIR ${3rdparty}/share/OpenCV)
#FIND_PACKAGE(OpenCV REQUIRED)

set(OpenCV_LIBS opencv_highgui opencv_features2d opencv_flann opencv_imgproc opencv_core)

#set(BOOST_ROOT ${3rdparty}/include)
#set(Boost_USE_STATIC_LIBS OFF)
#set(Boost_USE_MULTITHREADED ON)
#set(Boost_USE_STATIC_RUNTIME OFF)
#find_package(Boost REQUIRED program_options filesystem system serialization timer)
#find_package(Boost REQUIRED filesystem system)
#set(Boost_LIBRARIES boost_filesystem boost_system)
set(Boost_LIBRARIES boost_regex.a boost_system.a boost_filesystem.a boost_thread.a rt)
#execute_process(COMMAND ${CMAKE_COMMAND} -E make_directory bin)
add_definitions(-std=c++11)
add_definitions(-O2)
include_directories(${3rdparty}/include)
link_directories(${3rdparty}/lib)
link_directories(${3rdparty}/lib64)

include_directories("${PROJECT_SOURCE_DIR}/../include")
include_directories("${PROJECT_SOURCE_DIR}/../src/")
link_directories(${PROJECT_SOURCE_DIR}/../libs/)

link_directories(${PROJECT_SOURCE_DIR}/../libs/)

#素材指纹的service依赖的cpp文件
set(adsimilarity_srcs
    ../src/caffe_model_config.cpp
    ../src/json_serialize.cpp
    ../src/ad_similarity_classify.cpp
)

add_library(adsimilarity_services SHARED ${adsimilarity_srcs})
target_link_libraries(adsimilarity_services
            -Wl,--no-undefined
            -Wl,--whole-archive
            libpng
            libtiff
            libjpeg
            caffe.a
            -Wl,--no-whole-archive
            ${OpenCV_LIBS}
            ${Boost_LIBRARIES}
            pthread
            zlib.a
            protobuf.a
            openblas.a
            jsoncpp.a 
            #glog.a 
            #gflags.a
            -static-libstdc++ 
            -static-libgcc
            dl
            cudart_static.a 
            cublas_static.a 
            curand_static.a 
            culibos.a
            rt
        )

#-Wl,-Bsymbolic  #fixed protobuf double free
target_link_libraries(adsimilarity_services caffe.a  ${OpenCV_LIBS} ${Boost_LIBRARIES} pthread protobuf.a jsoncpp.a openblas.a)
#编译出libad_similarity_services.so
install(TARGETS adsimilarity_services DESTINATION libs)

file(GLOB_RECURSE c_api_tools RELATIVE ${PROJECT_SOURCE_DIR}/  ../c_api/*.cpp ../c_api/*.cc ../c_api/*.c)

#将c_api中的文件编译成可执行文件dynamic_load_nsfw_so, dynamic_load_beauty_so
#
foreach(f ${c_api_tools})
    get_filename_component(name ${f} NAME_WE)
    add_executable(${name} ${f})
    target_link_libraries(${name} pthread gflags.a jsoncpp.a dl -static-libstdc++ -static-libgcc)
    install(TARGETS  ${name} RUNTIME DESTINATION bin)
endforeach(f)


