#include <iostream>
#include <string>
#include <dlfcn.h>
#include <fstream>
#include <vector>
// #include <gflags/gflags.h>
typedef struct _AdSimilarityEngine {
    intptr_t p_handle;
} AdSimilarityEngine;

typedef AdSimilarityEngine *(*CreateAdSimilarityEngine)(std::string model_config, int  gpu_id);
typedef std::string (*AdSimilarityEnginePredict)(AdSimilarityEngine *p_engine, const std::vector<std::string> &image_buffer, int type);
typedef void (*ReleaseAdSimilarityEngine)(AdSimilarityEngine *p_engine);

CreateAdSimilarityEngine F_CREATE_AD_SIMILARITY_ENGINE;
AdSimilarityEnginePredict F_AD_SIMILARITY_PREDICT;
ReleaseAdSimilarityEngine F_RELEASE_AD_SIMILARITY_ENGINE;

AdSimilarityEngine *G_AD_SIMILARITY_ENGINE;


int main(int argc, char **argv) {
    // ::google::ParseCommandLineFlags(&argc, &argv, true);
    std::string so_path = argv[1];
    std::cout << "Begin loading:" << so_path << std::endl;
    void *handle = dlopen(so_path.c_str(), RTLD_LAZY);
    if (!handle) {
        std::cout << "Cannot open library:" << dlerror() << std::endl;
        return 1;
    }
    std::cout << "Load success" << std::endl;
    
    std::cout << "Query CreateAdSimilarityEngine" << std::endl;
    F_CREATE_AD_SIMILARITY_ENGINE = (CreateAdSimilarityEngine) dlsym(handle, "CreateAdSimilarityEngine");
    if (dlerror()) {
        std::cout << "Cannot load symbol 'CreateAdSimilarityEngine'" << dlerror();
        dlclose(handle);
        return 1;
    }

    std::cout << "Query AdSimilarityEnginePredict" << std::endl;
    F_AD_SIMILARITY_PREDICT = (AdSimilarityEnginePredict) dlsym(handle, "AdSimilarityEnginePredict");
    if (dlerror()) {
        std::cout << "Cannot load symbol 'AdSimilarityEnginePredict'" << dlerror();
        dlclose(handle);
        return 1;
    }

    std::cout << "Query ReleaseAdSimilarityEngine" << std::endl;
    F_RELEASE_AD_SIMILARITY_ENGINE = (ReleaseAdSimilarityEngine) dlsym(handle, "ReleaseAdSimilarityEngine");
    if (dlerror()) {
        std::cout << "Cannot load symbol 'ReleaseAdSimilarityEngine'" << dlerror();
        dlclose(handle);
        return 1;
    }

    // call CreateAdSimilarityEngine
    std::string ad_similarity_model = argv[2];
    int gpu_id = atoi(argv[4]);

    std::cout << "Call CreateAdSimilarityEngine" << std::endl;
    G_AD_SIMILARITY_ENGINE = F_CREATE_AD_SIMILARITY_ENGINE(ad_similarity_model, gpu_id);
    if (!G_AD_SIMILARITY_ENGINE) {
        std::cout <<  "Call 'CreateAdSimilarityEngine' fail: " << dlerror();
        return 1;
    }

    std::cout << "Call AdSimilarityEnginePredict" << std::endl;
    std::string file_name = argv[3];
    std::ifstream ifs(file_name);
    std::string image_buffer((std::istreambuf_iterator<char>(ifs)),
                             (std::istreambuf_iterator<char>()));
    std::vector<std::string> data_buffer;
    data_buffer.push_back(image_buffer);
    //type = 0:提取哈希特征
    std::string json_result_hash = F_AD_SIMILARITY_PREDICT(G_AD_SIMILARITY_ENGINE, data_buffer, 0);
    std::cout << "ad similarity hash Result:" << std::endl;
    std::cout << json_result_hash << std::endl;
    //type = 1:提取浮点数特征
    std::string json_result_float = F_AD_SIMILARITY_PREDICT(G_AD_SIMILARITY_ENGINE, data_buffer, 1);
    std::cout << "ad similarity float Result:" << std::endl;
    std::cout << json_result_float << std::endl;
    std::cout << "Call ReleaseAdSimilarityEngine" << std::endl;

    F_RELEASE_AD_SIMILARITY_ENGINE(G_AD_SIMILARITY_ENGINE);

    std::cout << "Done" << std::endl;

    return 0;
}
