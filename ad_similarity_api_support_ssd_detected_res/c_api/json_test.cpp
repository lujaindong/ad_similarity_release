#include <iostream>
#include <fstream>
#include "json/json.h"
#include "json/value.h"

void decode(){
    std::string text ="{ \"people\": [{\"id\": 1, \"name\":\"MIKE\",\"surname\":\"TAYLOR\"}, {\"id\": 2, \"name\":\"TOM\",\"surname\":\"JERRY\"} ]}";
    Json::Value root;
    Json::Reader reader;
    bool parsingSuccessful = reader.parse(text, root);
    if (!parsingSuccessful){
        std::cout << "Error parsing the string" << std::endl;
    }
    std::cout << root.size() << std::endl;
    const Json::Value mynames = root["people"];
    std::cout << root["people"].type() << std::endl;
    for (int index = 0; index < mynames.size(); ++index){
        //std::cout << mynames[index] << std::endl;
        std::cout << mynames[index]["id"] << " " << mynames[index]["name"] << " " << mynames[index]["surname"] << std::endl;
    }
}

void decode_from_file(){
    //std::ifstream ifile("/data2/leojacklu/素材指纹/ad_similarity_api_support_ssd_detected_res/json_test/ssd_detect.json", std::ifstream::binary);
    std::string text = "{\"data\":{\"errorcode\":0,\"errormsg\":\"\",\"image_height\":1390,\"image_width\":1100,\"latency\":169,\"regions\":[{\"bbox\":[351,335,789,335,789,1314,351,1314],\"confidence\":0.59434276819229126,\"name\":\"T\\u6064\",\"type\":18},{\"bbox\":[346,342,798,342,798,1377,346,1377],\"confidence\":0.15223097801208496,\"name\":\"\\u886c\\u886b\",\"type\":3},{\"bbox\":[348,329,785,329,785,1337,348,1337],\"confidence\":0.1284075528383255,\"name\":\"\\u7f8a\\u6bdb\\u8863\",\"type\":6}]}}";
    //std::string text = "{\"data\":{\"errorcode\":0,\"errormsg\":\"\",\"image_height\":870,\"image_width\":690,\"latency\":156,\"regions\":null}}"; 
    Json::Reader reader;
    Json::Value root;
    bool parsingSuccessful = reader.parse(text, root);
    if (!parsingSuccessful){
        std::cout << "Error parsing the string" << std::endl;
    }
    std::cout << root["data"]["errorcode"] << std::endl;
    std::cout << root["data"]["errormsg"] << std::endl;
    std::cout << root["data"]["image_height"] << std::endl;
    Json::Value regions = root["data"]["regions"];
    std::cout << "region size = " << regions.size() << std::endl;
    for(int i = 0; i < regions.size(); i++){
        std::cout << regions[i]["name"] << std::endl;
        int size = regions[i]["bbox"].size();
        for(int j = 0; j < size; j++)
            std::cout << regions[i]["bbox"][j] << " " << std::endl;
        std::cout << std::endl;
        root["data"]["regions"][i]["res_float"].append(1.0);
        root["data"]["regions"][i]["res_float"].append(2.0);
        root["data"]["regions"][i]["res_hash"] = "";
        root["data"]["regions"][i]["hex_bit"] = "";
    }
    std::cout << root << std::endl;
}

void convert(){
    //std::ifstream ifile("/data2/leojacklu/素材指纹/ad_similarity_api_support_ssd_detected_res/json_test/ssd_detect.json", std::ifstream::binary);
    std::string text = "{\"data\":{\"errorcode\":0,\"errormsg\":\"\",\"image_height\":1390,\"image_width\":1100,\"latency\":169,\"regions\":[{\"bbox\":[351,335,789,335,789,1314,351,1314],\"confidence\":0.59434276819229126,\"name\":\"T\\u6064\",\"type\":18},{\"bbox\":[346,342,798,342,798,1377,346,1377],\"confidence\":0.15223097801208496,\"name\":\"\\u886c\\u886b\",\"type\":3},{\"bbox\":[348,329,785,329,785,1337,348,1337],\"confidence\":0.1284075528383255,\"name\":\"\\u7f8a\\u6bdb\\u8863\",\"type\":6}]}}";
    //std::string text = "{\"data\":{\"errorcode\":0,\"errormsg\":\"\",\"image_height\":870,\"image_width\":690,\"latency\":156,\"regions\":null}}"; 
    Json::Reader reader;
    Json::Value root;
    bool parsingSuccessful = reader.parse(text, root);
    if (!parsingSuccessful){
        std::cout << "Error parsing the string" << std::endl;
    }
    Json::Value convert_root;
    const Json::Value regions = root["data"]["regions"];
    std::cout << "region size = " << regions.size() << std::endl;
    
    Json::Value json_regions;
    for(int i = 0; i < regions.size(); i++){
        Json::Value item;
        item["bbox"] = regions[i]["bbox"];
        item["confidence"] = regions[i]["confidence"];
        item["name"] = regions[i]["name"]; 
        item["type"] = regions[i]["type"];
        json_regions.append(item); 
    }
    Json::Value data_item;
    data_item["region_fingerprint"] = json_regions;
    convert_root["data"] = data_item;
    std::cout << convert_root << std::endl;
}


void json_insert(){
    Json::Value arrayObj;
    Json::Value new_item, new_item1;
    new_item["date"] = "2011-12-28";
    new_item1["time"] = "22:30:36";
    arrayObj.append(new_item);
    arrayObj.append(new_item1);
    std::cout << arrayObj << std::endl;
}

int main(int argc, char **argv){
    //decode();
    decode_from_file();
    //json_insert();
    //convert();
}
