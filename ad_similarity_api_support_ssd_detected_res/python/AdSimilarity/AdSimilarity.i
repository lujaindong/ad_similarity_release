%module AdSimilarity 
%{
#define SWIG_FILE_WITH_INIT
#include <string>
#include <memory>
#include <vector>
#include "types.h"
#include "ad_similarity_classify.h"

%}
%include "numpy.i"
%init 
%{
import_array();
%}
%include <std_string.i>
%include <std_vector.i>
namespace std {
  %template(strVector) vector<string>;
};
%include "types.h"
%include "ad_similarity_classify.h"
